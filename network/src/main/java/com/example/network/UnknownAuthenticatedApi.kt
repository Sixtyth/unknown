package com.example.network

import com.example.network.repository.AdsRepository
import com.example.network.response.DelayResponse
import com.example.network.response.EmptyResponse
import com.example.network.response.UpStreamEntity
import com.example.network.response.UserResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface UnknownAuthenticatedApi {

}