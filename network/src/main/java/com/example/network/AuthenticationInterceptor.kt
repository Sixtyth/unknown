package com.example.network

import okhttp3.Interceptor
import okhttp3.Response

class AuthenticationInterceptor(private val accessTokenSupplier: Supplier<String>) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request()
            .newBuilder()
            .header("Authorization", accessTokenSupplier.get())
            .build()
        return chain.proceed(newRequest)
    }
}