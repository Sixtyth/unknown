package com.example.network.response

import com.google.gson.annotations.SerializedName

data class DelayResponse(
    @SerializedName("page") val page: Int,
    @SerializedName("per_page") val perPage: Int,
    @SerializedName("total") val total: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("data") val data: List<DelayData>,
    @SerializedName("ad") val ad: DelayAds
)

data class DelayData(
    @SerializedName("id") val id: Int,
    @SerializedName("email") val email: String,
    @SerializedName("first_name") val firstName: String,
    @SerializedName("last_name") val lastName: String,
    @SerializedName("avatar") val avatar: String
)

data class DelayAds(
    val company: String,
    val url: String,
    val text: String
)