package com.example.network.response

data class DownStreamEntity<D, AD>(val data: D?, val ads: AD)