package com.example.network.response

import com.google.gson.annotations.SerializedName

data class UnknownResponse (@SerializedName("result") val result: String,
                            @SerializedName("data") val data: UserResponse)