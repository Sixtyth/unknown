package com.example.network.response

class APIException(message: String, val statusCode: Int?, val failureCode: String?) : Exception(message)