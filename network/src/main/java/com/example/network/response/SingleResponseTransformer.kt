package com.example.network.response

import io.reactivex.Single
import io.reactivex.SingleTransformer
import retrofit2.HttpException

class SingleResponseTransformer<D, AD> :
    SingleTransformer<UpStreamEntity<D, AD>, DownStreamEntity<D, AD>> {
    override fun apply(upstream: Single<UpStreamEntity<D, AD>>): Single<DownStreamEntity<D, AD>> {
        return upstream.map { (page, perPage, total, totalPages, data, ad,  error) ->
            if (error == null && data == null) {
                upstream.onErrorReturn {
                    throw APIException("${it.stackTrace}", (it as HttpException).code(), it.message())
                }
            }
            DownStreamEntity(data, ad!!)
        }
    }
}

