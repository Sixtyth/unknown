package com.example.network.response

import com.google.gson.annotations.SerializedName

data class UpStreamEntity<D, AD>(@SerializedName("page") val page: Int?,
                                 @SerializedName("per_page") val perPage: Int?,
                                 @SerializedName("total") val total: Int?,
                                 @SerializedName("total_pages") val totalPages: Int?,
                                 @SerializedName("data") val data: D?,
                                 @SerializedName("ad") val ad: AD?,
                                 @SerializedName("error") val error: String?)