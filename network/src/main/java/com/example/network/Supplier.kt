package com.example.network

interface Supplier<out T> {
    fun get() : T
}