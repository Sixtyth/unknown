package com.example.network

import retrofit2.HttpException
import java.lang.Exception
import java.net.SocketTimeoutException

open class ResponseHandler {

    fun <T : Any> handleSuccess(data: T): Resource<T> {
        return Resource.success(data)
    }

    fun <T : Any> handleException(e: Exception): Resource<T> {
        return when (e) {
            is HttpException -> Resource.error(getErrorMessage(e.code()), null)
            is SocketTimeoutException -> Resource.error(getErrorMessage(ErrorCodes.SocketTimeOut.code), null)
            else -> Resource.error(getErrorMessage(Int.MAX_VALUE), null)
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            ErrorCodes.SocketTimeOut.code -> "Timeout"
            401 -> "Unauthorised"
            404 -> "Not found"
            else -> "Something went wrong"
        }
    }
}

enum class ErrorCodes(val code: Int) {
    SocketTimeOut(-1)
}

enum class ErrorMessage(val msg: String) {
    TIME_OUT("Timeout"),
    UNAUTHORISED("Unauthorised"),
    NOT_FOUND("Not found"),
    SOMETHING_WENT_WRONG("Something went wrong");

    companion object {
        fun from(findValue: String): ErrorMessage = values().first { it.msg == findValue }
    }
}