package com.example.network

import com.example.network.repository.AdsRepository
import com.example.network.response.DelayResponse
import com.example.network.response.EmptyResponse
import com.example.network.response.UpStreamEntity
import com.example.network.response.UserResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface UnknownApi {

    @GET("api/users")
    fun getAllUser(@Query("page") page: Int): Single<UpStreamEntity<List<UserResponse>, AdsRepository>>

    @GET("api/users")
    fun getAllUserDelay(@Query("delay") delay: Int): Single<UpStreamEntity<List<UserResponse>, AdsRepository>>

    @GET("api/users/23")
    fun getSingleUserNotFound(): Single<UpStreamEntity<EmptyResponse, EmptyResponse>>

    @GET("users")
    fun login(@Query("delay") user: Int): Single<UpStreamEntity<DelayResponse, AdsRepository>>
}