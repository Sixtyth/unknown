package com.example.network.dataSource

import com.example.network.repository.AdsRepository
import com.example.network.response.DelayResponse
import com.example.network.response.DownStreamEntity
import com.example.network.response.UserResponse
import io.reactivex.Single

interface AuthenticationDataSource {

    fun login(delay: Int): Single<DownStreamEntity<DelayResponse, AdsRepository>>

}