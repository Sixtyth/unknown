package com.example.network.dataSource

import com.example.network.repository.AdsRepository
import com.example.network.response.DownStreamEntity
import com.example.network.response.EmptyResponse
import com.example.network.response.UserResponse
import io.reactivex.Single

interface UserDataSource {

    fun getAllUser(page: Int): Single<DownStreamEntity<List<UserResponse>, AdsRepository>>

    fun getAllUserWithDelay(delay: Int): Single<DownStreamEntity<List<UserResponse>, AdsRepository>>

    fun getSingleUserNotFound(): Single<DownStreamEntity<EmptyResponse, EmptyResponse>>

}