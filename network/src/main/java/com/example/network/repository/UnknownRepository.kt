package com.example.network.repository

import com.example.network.dataSource.AuthenticationDataSource
import com.example.network.UnknownApi
import com.example.network.dataSource.UserDataSource
import com.example.network.response.*
import io.reactivex.Single

open class UnknownRepository(private val unknownApi: UnknownApi) : AuthenticationDataSource,
    UserDataSource {

    /** Ref @link http://www.mocky.io/v2/5e737540300000d5512e65a7 */
    override fun getAllUser(page: Int): Single<DownStreamEntity<List<UserResponse>, AdsRepository>> =
        unknownApi.getAllUser(page).compose(SingleResponseTransformer())

    override fun getAllUserWithDelay(delay: Int): Single<DownStreamEntity<List<UserResponse>, AdsRepository>> =
        unknownApi.getAllUserDelay(delay).compose(SingleResponseTransformer())

    override fun getSingleUserNotFound(): Single<DownStreamEntity<EmptyResponse, EmptyResponse>> =
        unknownApi.getSingleUserNotFound().compose(SingleResponseTransformer())

    override fun login(delay: Int): Single<DownStreamEntity<DelayResponse, AdsRepository>> =
        unknownApi.login(delay).compose(SingleResponseTransformer())

}