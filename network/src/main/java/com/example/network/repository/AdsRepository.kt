package com.example.network.repository

import com.google.gson.annotations.SerializedName

data class AdsRepository(
    @SerializedName("company") val company: String?,
    @SerializedName("url") val url: String?,
    @SerializedName("text") val text: String?
)