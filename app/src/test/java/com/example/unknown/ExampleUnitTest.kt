package com.example.unknown

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun getFibonacci() {
        //printing Fibonacci series upto number
        for (index in 1..10) {
            println("${fibonacci2(index)} ")
        }
    }

    /** Java program for Fibonacci number using recursion. * This program uses tail recursion to calculate Fibonacci number for a given number * @return Fibonacci number */
    private fun fibonacci(number: Int): Int {
        if (number == 1 || number == 2) {
            return 1
        }
        return fibonacci(number - 1) + fibonacci(number - 2)
        //tail recursion
        /** Read more: https://www.java67.com/2016/05/fibonacci-series-in-java-using-recursion.html#ixzz6HWNuGoGc*/
    }

    /* * Java program to calculate Fibonacci number using loop or Iteration. * @return Fibonacci number */
    private fun fibonacci2(number: Int): Int {
        if (number == 1 || number == 2) {
            return 1
        }
        var fibo1 = 1
        var fibo2 = 1
        var fibonacci = 1
        for (i in 3 until number) {
            //Fibonacci number is sum of previous two Fibonacci number
            fibonacci = fibo1 + fibo2
            fibo1 = fibo2
            fibo2 = fibonacci
        }
        return fibonacci //Fibonacci number
    }
}
