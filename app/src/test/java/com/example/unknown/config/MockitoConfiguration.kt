package com.example.unknown.config

import androidx.annotation.NonNull
import io.reactivex.Observable
import io.reactivex.Single
import org.mockito.configuration.DefaultMockitoConfiguration
import org.mockito.internal.stubbing.defaultanswers.ReturnsEmptyValues
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer

class MockitoConfiguration: DefaultMockitoConfiguration() {

    override fun getDefaultAnswer(): Answer<Any?>? {
        return object : ReturnsEmptyValues() {
            override fun answer(inv: InvocationOnMock): Any {
                val type = inv.method.returnType
                return when {
                    type.isAssignableFrom(Observable::class.java) -> {
                        Observable.error<RuntimeException>(createException(inv))
                    }
                    type.isAssignableFrom(Single::class.java) -> {
                        Single.error<RuntimeException>(createException(inv))
                    }
                    else -> {
                        super.answer(inv)
                    }
                }
            }
        }
    }

    @NonNull
    private fun createException(
        invocation: InvocationOnMock
    ): RuntimeException {
        val s = invocation.toString()
        return RuntimeException(
            "No mock defined for invocation $s"
        )
    }

}