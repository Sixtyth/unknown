package com.example.unknown.suite

import com.example.unknown.datasource.GetAllUserDataSourceTest
import com.example.unknown.datasource.GetAllUserWithDelayDataSourceTest
import com.example.unknown.datasource.GetSingleUserNotFoundDataSourceTest
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.Suite.SuiteClasses


@RunWith(Suite::class)
@SuiteClasses(GetAllUserDataSourceTest::class, GetAllUserWithDelayDataSourceTest::class, GetSingleUserNotFoundDataSourceTest::class)
class UserDataSourceTestSuite {

}