package com.example.unknown.screen

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.util.regex.Pattern

@RunWith(JUnit4::class)
class AuthenticationActivityTest {

    private var user: String? = "Sixtyth66@gmail.com"
    private var pass: String? = "123456"

    @Test
    fun testUserNameIsNullAndBlank() {
        user = null
        assertEquals(false, validateDataInput(user, pass))

        user = ""
        assertEquals(false, validateDataInput(user, pass))
    }

    @Test
    fun testPasswordIsNull() {
        pass = null
        assertEquals(false, validateDataInput(user, pass))

        pass = ""
        assertEquals(false, validateDataInput(user, pass))
    }

    @Test
    fun testUserAndPassNotNull_UserIsEmailFormat() {
        if (validateDataInput(user, pass)) {
            println("Username password is notnull and User is email format")
        } else {
            println("Username password is notnull and User isn't email format")
        }

        assertEquals(true, validateDataInput(user, pass))
    }

    @Test
    fun testUserAndPassNotNull_UserIsNotEmailFormat() {
        user = "Sixtyth@gmail..com"
        if (validateDataInput(user, pass)) {
            println("Username password is notnull and User is email format")
        } else {
            println("Username password is notnull and User isn't email format")
        }

        assertEquals(false, validateDataInput(user, pass))
    }

    private fun validateDataInput(user: String?, pass: String?): Boolean {
        return if (isValidUserNameAndPasswordInput(user, pass)) {
            isValidEmailFormat(user)
        } else {
            false
        }
    }

    private fun isValidUserNameAndPasswordInput(user: String?, pass: String?): Boolean =
        !(user.isNullOrEmpty() || user.isNullOrBlank() || pass.isNullOrEmpty() || pass.isNullOrBlank())

    private fun isValidEmailFormat(email: String?): Boolean {
        val emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$"
        val pat = Pattern.compile(emailRegex)
        return if (email == null) false else pat.matcher(email).matches()
    }

}