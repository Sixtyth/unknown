package com.example.unknown.screen

import com.example.unknown.utils.UserInfo
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PinCodeActivityTest {

    private var pinCode: String? = "1234"
    private var storePinCode: String? = "4321"
    private var hasCreatePinCode = false

    @Test
    fun getPinCodeNotNullAndEmptyAndBlank() {
        assertNotNull(pinCode)
    }

    @Test
    fun getPinCodeNotEmpty() {
        assertNotEquals("", pinCode)
    }

    @Test
    fun getPinCodeNotBlank() {
        assertNotEquals(" ", pinCode)
    }

    @Test
    fun getPinCodeNull() {
        pinCode = null
        assertNull(pinCode)
    }

    @Test
    fun getPinCodeEmpty() {
        pinCode = ""
        assertEquals("", pinCode)
    }

    @Test
    fun getPinCodeBlank() {
        assertNotEquals(" ", pinCode)
    }

    @Test
    fun isValidPinCodeWithOutHasCreatePinCodeSuccess() {
        pinCode = "1234"
        storePinCode = "1234"
        assertTrue(isValidPinCode(hasCreatePinCode, storePinCode!!, pinCode!!))
    }

    @Test
    fun isValidPinCodeWithOutHasCreatePinCodeFail() {
        assertFalse(isValidPinCode(hasCreatePinCode, storePinCode!!, pinCode!!))
    }

    @Test
    fun isValidPinCodeWithHasCreatePinCodeSuccess() {
        hasCreatePinCode = true
        pinCode = "1234"
        storePinCode = "1234"
        assertTrue(isValidPinCode(hasCreatePinCode, storePinCode!!, pinCode!!))
    }

    @Test
    fun isValidPinCodeWithHasCreatePinCodeFail() {
        hasCreatePinCode = true
        assertFalse(isValidPinCode(hasCreatePinCode, storePinCode!!, pinCode!!))
    }

    @Test
    fun storeThePinCodeSuccess() {
        storePinCode = pinCode
        assertEquals(storePinCode, pinCode)
    }

    @Test
    fun storeThePinCodeFail() {
        assertNotEquals(storePinCode, pinCode)
    }

    @Test
    fun removeLastPinCodeWithHaveSomeDigit() {
        val lastDataRemove: String? = removeLastCharacter(pinCode)
        assertNotNull(lastDataRemove)
    }

    @Test
    fun removeLastPinCodeReturnEmpty() {
        pinCode = "1"
        val lastDataRemove: String? = removeLastCharacter(pinCode)
        assertEquals("", lastDataRemove)
    }

    private fun removeLastCharacter(str: String?): String? {
        var result: String? = null
        if (str != null && str.isNotEmpty()) {
            result = str.substring(0, str.length - 1)
        }
        return result
    }

    private fun isValidPinCode(hasCreate: Boolean, storePinCode: String, pinCode: String): Boolean {
        return if (hasCreate) {
            storePinCode == pinCode
        } else {
            storePinCode == pinCode
        }
    }

}