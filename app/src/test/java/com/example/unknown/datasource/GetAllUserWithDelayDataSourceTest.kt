package com.example.unknown.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.network.ErrorMessage
import com.example.network.Resource
import com.example.network.ResponseHandler
import com.example.network.repository.UnknownRepository
import com.example.network.response.APIException
import com.example.network.response.DownStreamEntity
import com.example.unknown.config.RxSchedulerRule
import com.example.unknown.dummy.adsResponse
import com.example.unknown.dummy.apiException404
import com.example.unknown.dummy.apiExceptionUnknownError
import com.example.unknown.dummy.userResponse
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.HttpException
import retrofit2.Response

class GetAllUserWithDelayDataSourceTest {

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Mock
    var apiClient: UnknownRepository? = null

    @Before
    fun createRepository() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun getAllUserWithDelaySuccess_requestsAllUserFromRemoteDataSource() {

        // Mock API response
        Mockito.`when`(apiClient!!.getAllUserWithDelay(ArgumentMatchers.anyInt())).thenReturn(
            Single.just(
                DownStreamEntity(
                    listOf(userResponse),
                    adsResponse
                )
            )
        )

        // Then tasks are loaded from the remote data source
        apiClient!!.getAllUserWithDelay(ArgumentMatchers.anyInt())
            .subscribe({
                assertNotNull(it.data)
                assertNotNull(it.ads)
            }, {

            })
    }

    @Test
    fun getAllUserWithDelayError404_requestsAllUserFromRemoteDataSource() {
        /* Mock error 404 API Response */
        Mockito.`when`(apiClient!!.getAllUserWithDelay(ArgumentMatchers.anyInt())).thenReturn(
            Single.error(
                apiException404
            )
        )

        apiClient!!.getAllUserWithDelay(ArgumentMatchers.anyInt()).subscribe({
        }, {
            if (it is APIException) {
                val errorCode: Resource<String> = ResponseHandler().handleException(
                    HttpException(
                        Response.error<String>(
                            it.statusCode ?: Int.MAX_VALUE,
                            "{}".toResponseBody("application/json".toMediaTypeOrNull())
                        )
                    )
                )
                Assert.assertEquals(ErrorMessage.NOT_FOUND, ErrorMessage.from(errorCode.message!!))
            }
        })
    }

    @Test
    fun getAllUserWithDelaySomethingWentWrong_requestsAllUserFromRemoteDataSource() {
        Mockito.`when`(apiClient!!.getAllUserWithDelay(ArgumentMatchers.anyInt())).thenReturn(
            Single.error(
                apiExceptionUnknownError
            )
        )

        apiClient!!.getAllUserWithDelay(ArgumentMatchers.anyInt()).subscribe({

        }, {
            val errorCode = ResponseHandler().handleException<String>(it as Exception)
            Assert.assertEquals(
                ErrorMessage.SOMETHING_WENT_WRONG,
                ErrorMessage.from(errorCode.message!!)
            )
        })
    }

}