package com.example.unknown.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.network.ErrorMessage
import com.example.network.Resource
import com.example.network.ResponseHandler
import com.example.network.repository.UnknownRepository
import com.example.network.response.APIException
import com.example.unknown.config.RxSchedulerRule
import com.example.unknown.dummy.apiException404
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.HttpException
import retrofit2.Response

class GetSingleUserNotFoundDataSourceTest {

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Mock
    var apiClient: UnknownRepository? = null

    @Before
    fun createRepository() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun getSingleUserNotFound_requestsSingleUserNotFoundFromRemoteDataSource() {
        /* Mock Error Api */
        Mockito.`when`(apiClient!!.getSingleUserNotFound()).thenReturn(
            Single.error(
                apiException404
            )
        )

        // Then tasks are loaded from the remote data source
        apiClient!!.getSingleUserNotFound().subscribe({

        }, {
            if (it is APIException) {
                val errorCode: Resource<String> = ResponseHandler().handleException(
                    HttpException(
                        Response.error<String>(
                            it.statusCode ?: Int.MAX_VALUE,
                            "{}".toResponseBody("application/json".toMediaTypeOrNull())
                        )
                    )
                )
                Assert.assertEquals(ErrorMessage.NOT_FOUND, ErrorMessage.from(errorCode.message!!))
            }
        })
    }

    @Test
    fun getSingleUserSomeThingWentWrong_requestSingleUserSomethingWentWrongFromRemoteDataSource() {
        /* Mock api something went wrong */
        Mockito.`when`(apiClient!!.getSingleUserNotFound()).thenReturn(
            Single.error(
                APIException(
                    "Something Went Wrong",
                    Int.MAX_VALUE,
                    "Something Went Wrong"
                )
            )
        )

        // Then tasks are loaded from the remote data source
        apiClient!!.getSingleUserNotFound().subscribe({

        }, {
            val error = ResponseHandler().handleException<String>(it as Exception)
            Assert.assertEquals(
                ErrorMessage.SOMETHING_WENT_WRONG,
                ErrorMessage.from(error.message!!)
            )
        })

    }

}