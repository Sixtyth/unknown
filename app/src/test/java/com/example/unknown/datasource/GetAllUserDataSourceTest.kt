/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.unknown.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.network.ErrorMessage
import com.example.network.Resource
import com.example.network.ResponseHandler
import com.example.network.repository.UnknownRepository
import com.example.network.response.APIException
import com.example.network.response.DownStreamEntity
import com.example.unknown.config.RxSchedulerRule
import com.example.unknown.dummy.adsResponse
import com.example.unknown.dummy.apiException404
import com.example.unknown.dummy.apiExceptionUnknownError
import com.example.unknown.dummy.userResponse
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyInt
import org.mockito.MockitoAnnotations
import retrofit2.HttpException
import retrofit2.Response
import java.util.concurrent.TimeUnit


/**
 * Unit tests for the implementation of the in-memory repository with cache.
 */
class GetAllUserDataSourceTest {

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Mock
    var apiClient: UnknownRepository? = null

    @Before
    fun createRepository() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun getAllUserSuccess_requestsAllUserFromRemoteDataSource() {

        // Mock API response
        `when`(apiClient!!.getAllUser(ArgumentMatchers.anyInt())).thenReturn(
            Single.just(
                DownStreamEntity(
                    listOf(userResponse),
                    adsResponse
                )
            )
        )

        // Then tasks are loaded from the remote data source
        apiClient!!.getAllUser(ArgumentMatchers.anyInt()).subscribe({
            assertNotNull(it.data)
            assertNotNull(it.ads)
        }, {

        })
    }

    @Test
    fun getAllUserError404_requestsAllUserFromRemoteDataSource() {
        /* Mock error 404 API Response */
        `when`(apiClient!!.getAllUser(ArgumentMatchers.anyInt())).thenReturn(
            Single.error(
                apiException404
            )
        )

        apiClient!!.getAllUser(ArgumentMatchers.anyInt()).subscribe({
        }, {
            if (it is APIException) {
                val errorCode: Resource<String> = ResponseHandler().handleException(
                    HttpException(
                        Response.error<String>(
                            it.statusCode ?: Int.MAX_VALUE,
                            "{}".toResponseBody("application/json".toMediaTypeOrNull())
                        )
                    )
                )
                assertEquals(ErrorMessage.NOT_FOUND, ErrorMessage.from(errorCode.message!!))
            }
        })
    }

    @Test
    fun getAllUserSomethingWentWrong_requestsAllUserFromRemoteDataSource() {
        `when`(apiClient!!.getAllUser(ArgumentMatchers.anyInt())).thenReturn(Single.error(
            apiExceptionUnknownError)
        )

        apiClient!!.getAllUser(ArgumentMatchers.anyInt()).subscribe({

        }, {
            val errorCode = ResponseHandler().handleException<String>(it as Exception)
            assertEquals(ErrorMessage.SOMETHING_WENT_WRONG, ErrorMessage.from(errorCode.message!!))
        })
    }

}

