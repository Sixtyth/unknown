package com.example.unknown.dummy

import com.example.network.response.UserResponse

val userResponse = UserResponse(
    id = 7,
    email = "michael.lawson@reqres.in",
    firstName = "Michael",
    lastName = "Lawson",
    avatar = "https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg"
)