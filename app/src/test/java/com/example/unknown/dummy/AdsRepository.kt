package com.example.unknown.dummy

import com.example.network.repository.AdsRepository

val adsResponse = AdsRepository(
    company = "StatusCode Weekly",
    url = "http://statuscode.org/",
    text =
    "A weekly newsletter focusing on software development, infrastructure, the server, performance, and the stack end of things."
)