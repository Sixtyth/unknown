package com.example.unknown.dummy

import com.example.network.response.APIException

val apiException404 = APIException(
    "Not found",
    404,
    "Not found"
)

val apiExceptionUnknownError = APIException(
    "Something went wrong",
    Int.MAX_VALUE,
    "Something went wrong"
)