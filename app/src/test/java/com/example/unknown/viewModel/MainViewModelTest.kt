package com.example.unknown.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.network.UnknownApi
import com.example.network.repository.AdsRepository
import com.example.network.repository.UnknownRepository
import com.example.network.response.APIException
import com.example.network.response.DownStreamEntity
import com.example.network.response.UserResponse
import com.example.unknown.config.RxSchedulerRule
import com.example.unknown.config.getOrAwaitValue
import com.example.unknown.viewModel.main.MainViewModel
import io.reactivex.Single
import org.hamcrest.Matchers.not
import org.hamcrest.Matchers.nullValue
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class MainViewModelTest {

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Mock
    var apiEndPoint: UnknownApi? = null

    @Mock
    var apiClient: UnknownRepository? = null

    private var viewModel: MainViewModel? = null

    @Mock
    var observer: Observer<MainViewModel.Status>? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = MainViewModel(apiClient!!)
        viewModel!!.status.observeForever(observer!!)
    }

    @Test
    fun testNullApiClient() {
        assertNotNull(apiClient)
    }

    @Test
    fun testNull() {
        `when`(apiClient!!.getAllUser(ArgumentMatchers.anyInt())).thenReturn(null)
        assertNotNull(viewModel!!.status)
        assertTrue(viewModel!!.status.hasObservers())
    }

    @Test
    fun testApiFetchDataSuccess() {
        // Mock API response
        `when`(apiClient!!.getAllUser(ArgumentMatchers.anyInt())).thenReturn(
            Single.just(
                DownStreamEntity(
                    listOf(
                        UserResponse(
                            id = 7,
                            email = "michael.lawson@reqres.in",
                            firstName = "Michael",
                            lastName = "Lawson",
                            avatar = "https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg"
                        )
                    )
                    , AdsRepository(
                        company = "StatusCode Weekly",
                        url = "http://statuscode.org/",
                        text =
                        "A weekly newsletter focusing on software development, infrastructure, the server, performance, and the stack end of things."
                    )
                )
            )
        )

        viewModel!!.getAllUser()

        println("Fetch : ${viewModel!!.status.value!!.viewDataBundle.userResponse}")

        val value = viewModel!!.status.getOrAwaitValue()

        assertThat(value.viewDataBundle, not(nullValue()))
    }

    @Test
    fun testApiFetchDataError() {
        `when`(apiClient!!.getAllUser(ArgumentMatchers.anyInt()))
            .thenReturn(Single.error(APIException("Error", 404, "Error")))

        viewModel!!.getAllUser()

        val value = viewModel!!.status.getOrAwaitValue()

        verify(observer, times(1))!!.onChanged(
            ArgumentMatchers.any(MainViewModel.Status.Error::class.java)
        )

        assertThat(value.viewDataBundle, not(nullValue()))
    }

    @After
    @Throws(java.lang.Exception::class)
    fun tearDown() {
        apiClient = null
        viewModel = null
    }

}