package com.example.unknown.screen.main

import android.view.Menu
import android.view.MenuItem
import androidx.test.annotation.UiThreadTest
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.filters.SmallTest
import androidx.test.rule.ActivityTestRule
import com.example.unknown.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemReselectedListener
import org.hamcrest.core.AllOf.allOf
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activityTestRule = ActivityTestRule(
        MainActivity::class.java
    )

    private val MENU_CONTENT_ITEM_IDS = intArrayOf(
        R.id.item_home, R.id.item_empty_one, R.id.item_empty_two, R.id.item_setting
    )

    private var mBottomNavigation: BottomNavigationView? = null

    @Mock
    private val mockedListener: BottomNavigationView.OnNavigationItemSelectedListener? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        val activity: MainActivity = activityTestRule.activity
        mBottomNavigation = activity.findViewById(R.id.btnNavView)
    }

    @UiThreadTest
    @Test
    @SmallTest
    fun testAddItemsWithoutMenuInflation() {
        val navigation = BottomNavigationView(activityTestRule.activity)
        activityTestRule.activity.setContentView(navigation)
        navigation.menu.add("Item1")
        navigation.menu.add("Item2")
        assertEquals(2, navigation.menu.size())
        navigation.menu.removeItem(0)
        navigation.menu.removeItem(0)
        assertEquals(0, navigation.menu.size())
    }

    @Test
    @SmallTest
    fun testBasics() {
        // Check the contents of the Menu object
        val menu: Menu = mBottomNavigation!!.menu
        assertNotNull("Menu should not be null", menu)
        assertEquals(
            "Should have matching number of items",
            MENU_CONTENT_ITEM_IDS.size,
            menu.size()
        )
        for (i in MENU_CONTENT_ITEM_IDS.indices) {
            val currItem: MenuItem = menu.getItem(i)
            println("Current Item : $currItem $i")
            assertEquals("ID for Item #$i", MENU_CONTENT_ITEM_IDS[i], currItem.itemId)
        }
    }

    @Test
    @LargeTest
    fun testNavigationSelectionListener() {
        mBottomNavigation!!.setOnNavigationItemSelectedListener(mockedListener)

        // Make the listener return true to allow selecting the item.
        `when`(mockedListener!!.onNavigationItemSelected(any(MenuItem::class.java)))
            .thenReturn(true)

        onView(
            allOf(
                withId(R.id.item_home),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify our listener has been notified of the click
        verify(mockedListener, times(1))
            .onNavigationItemSelected(
                mBottomNavigation!!.menu.findItem(R.id.item_home)
            )

        // Verify the item is now selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_home).isChecked)

        // Select the same item again
        onView(
            allOf(
                withId(R.id.item_home),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify our listener has been notified of the click
        verify(mockedListener, times(2))
            .onNavigationItemSelected(mBottomNavigation!!.menu.findItem(R.id.item_home))

        // Verify the item is still selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_home).isChecked)

        onView(
            allOf(
                withId(R.id.item_empty_one),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify our listener has been notified of the click
        verify(mockedListener, times(1))
            .onNavigationItemSelected(mBottomNavigation!!.menu.findItem(R.id.item_empty_one))

        // Verify the item is still selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_empty_one).isChecked)

        // Make the listener return false to disallow selecting the item.
        `when`(mockedListener.onNavigationItemSelected(any(MenuItem::class.java)))
            .thenReturn(false)

        onView(
            allOf(
                withId(R.id.item_empty_two),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify our listener has been notified of the click
        verify(mockedListener, times(1))
            .onNavigationItemSelected(mBottomNavigation!!.menu.findItem(R.id.item_empty_two))

        // Verify the previous item is still selected
        assertFalse(mBottomNavigation!!.menu.findItem(R.id.item_empty_two).isChecked)
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_empty_one).isChecked)

        // Set null listener to test that the next click is not going to notify the
        // previously set listener and will allow selecting items.
        mBottomNavigation!!.setOnNavigationItemSelectedListener(null)

        // Click one of our items
        onView(
            allOf(
                withId(R.id.item_setting),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify that our previous listener has not been notified of the click
        verifyNoMoreInteractions(mockedListener)

        // Verify the correct item is now selected.
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_setting).isChecked)
    }

    @UiThreadTest
    @Test
    @SmallTest
    fun testSetSelectedItemId() {
        val mockedListener = mock(
            BottomNavigationView.OnNavigationItemSelectedListener::class.java
        )
        mBottomNavigation!!.setOnNavigationItemSelectedListener(mockedListener)

        // Make the listener return true to allow selecting the item.
        `when`(mockedListener.onNavigationItemSelected(any(MenuItem::class.java)))
            .thenReturn(true)

        // Programmatically select an item
        mBottomNavigation!!.selectedItemId = R.id.item_home

        // Verify our listener has been notified of the click
        verify(mockedListener, times(1))
            .onNavigationItemSelected(
                mBottomNavigation!!.menu.findItem(R.id.item_home)
            )

        // Verify the item is now selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_home).isChecked)

        // Select the same item
        mBottomNavigation!!.selectedItemId = R.id.item_home

        // Verify our listener has been notified of the click
        verify(mockedListener, times(2))
            .onNavigationItemSelected(
                mBottomNavigation!!.menu.findItem(R.id.item_home)
            )

        // Verify the item is still selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_home).isChecked)

        // Programmatically select an item
        mBottomNavigation!!.selectedItemId = R.id.item_empty_one

        // Verify our listener has been notified of the click
        verify(mockedListener, times(1))
            .onNavigationItemSelected(
                mBottomNavigation!!.menu.findItem(R.id.item_empty_one)
            )

        // Verify the item is now selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_empty_one).isChecked)

        // Make the listener return false to disallow selecting the item.
        `when`(mockedListener.onNavigationItemSelected(any(MenuItem::class.java)))
            .thenReturn(false)

        // Programmatically select an item
        mBottomNavigation!!.selectedItemId = R.id.item_empty_two

        // Verify our listener has been notified of the click
        verify(mockedListener, times(1))
            .onNavigationItemSelected(mBottomNavigation!!.menu.findItem(R.id.item_empty_two))

        // Verify the previous item is still selected
        assertFalse(mBottomNavigation!!.menu.findItem(R.id.item_empty_two).isChecked)
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_empty_one).isChecked)

        // Set null listener to test that the next click is not going to notify the
        // previously set listener and will allow selecting items.
        mBottomNavigation!!.setOnNavigationItemSelectedListener(null)

        // Select one of our items
        mBottomNavigation!!.selectedItemId = R.id.item_setting
        // Verify that our previous listener has not been notified of the click
        verifyNoMoreInteractions(mockedListener)
        // Verify the correct item is now selected.
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_setting).isChecked)
    }

    @Test
    @SmallTest
    fun testNavigationReselectionListener() {
        // Add an OnNavigationItemReselectedListener
        val reselectedListener = mock(
            OnNavigationItemReselectedListener::class.java
        )
        mBottomNavigation!!.setOnNavigationItemReselectedListener(reselectedListener)

        // Select an item
        onView(
            allOf(
                withId(R.id.item_home),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify the item is now selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_home).isChecked)

        // Verify the listener was not called
        verify(reselectedListener, times(1))
            .onNavigationItemReselected(any(MenuItem::class.java))

        // Select the same item again
        onView(
            allOf(
                withId(R.id.item_home),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify the item is still selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_home).isChecked)

        // Verify the listener was called
        verify(reselectedListener, times(2))
            .onNavigationItemReselected(
                mBottomNavigation!!.menu.findItem(R.id.item_home)
            )

        // Add an OnNavigationItemSelectedListener
        val selectedListener = mock(
            BottomNavigationView.OnNavigationItemSelectedListener::class.java
        )
        mBottomNavigation!!.setOnNavigationItemSelectedListener(selectedListener)

        // Make the listener return true to allow selecting the item.
        `when`(selectedListener.onNavigationItemSelected(any(MenuItem::class.java)))
            .thenReturn(true)

        // Select another item
        onView(
            allOf(
                withId(R.id.item_empty_one),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify the item is now selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_empty_one).isChecked)

        // Verify the correct listeners were called
        verify(selectedListener, times(1))
            .onNavigationItemSelected(mBottomNavigation!!.menu.findItem(R.id.item_empty_one))

        verify(reselectedListener, never())
            .onNavigationItemReselected(
                mBottomNavigation!!.menu.findItem(R.id.item_empty_one)
            )

        // Select the same item again
        onView(
            allOf(
                withId(R.id.item_empty_one),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        )
            .perform(click())
        // Verify the item is still selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_empty_one).isChecked)
        // Verify the correct listeners were called
        verifyNoMoreInteractions(selectedListener)
        verify(reselectedListener, times(1))
            .onNavigationItemReselected(
                mBottomNavigation!!.menu.findItem(R.id.item_empty_one)
            )

        // Add an OnNavigationItemSelectedListener
        val selectedFalseListener = mock(
            BottomNavigationView.OnNavigationItemSelectedListener::class.java
        )
        mBottomNavigation!!.setOnNavigationItemSelectedListener(selectedFalseListener)

        // Make the listener return true to allow selecting the item.
        `when`(selectedFalseListener.onNavigationItemSelected(any(MenuItem::class.java)))
            .thenReturn(false)

        // Select another item
        onView(
            allOf(
                withId(R.id.item_empty_two),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify the item is now selected
        assertFalse(mBottomNavigation!!.menu.findItem(R.id.item_empty_two).isChecked)
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_empty_one).isChecked)

        // Verify the correct listeners were called
        verify(selectedFalseListener, times(1))
            .onNavigationItemSelected(mBottomNavigation!!.menu.findItem(R.id.item_empty_two))

        // Select the same item again
        onView(
            allOf(
                withId(R.id.item_empty_two),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify the item is still selected
        assertFalse(mBottomNavigation!!.menu.findItem(R.id.item_empty_two).isChecked)

        verify(selectedFalseListener, times(2))
            .onNavigationItemSelected(
                mBottomNavigation!!.menu.findItem(R.id.item_empty_two)
            )

        // Make the listener return true to allow selecting the item.
        `when`(selectedFalseListener.onNavigationItemSelected(any(MenuItem::class.java)))
            .thenReturn(true)

        // Select another item
        onView(
            allOf(
                withId(R.id.item_setting),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify the item is now selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_setting).isChecked)

        // Verify the correct listeners were called
        verify(selectedFalseListener, times(1))
            .onNavigationItemSelected(mBottomNavigation!!.menu.findItem(R.id.item_setting))

        // Remove the OnNavigationItemReselectedListener
        mBottomNavigation!!.setOnNavigationItemReselectedListener(null)

        // Select the same item again
        onView(
            allOf(
                withId(R.id.item_setting),
                isDescendantOfA(withId(R.id.btnNavView)),
                isDisplayed()
            )
        ).perform(click())

        // Verify the reselectedListener was not called
        verifyNoMoreInteractions(reselectedListener)

        // Verify the item is still selected
        assertTrue(mBottomNavigation!!.menu.findItem(R.id.item_setting).isChecked)

    }

    @UiThreadTest
    @Test
    @SmallTest
    fun testSelectedItemIdWithEmptyMenu() {
        // First item initially selected
        assertEquals(R.id.item_home, mBottomNavigation!!.selectedItemId)

        // Remove all the items
        for (id in MENU_CONTENT_ITEM_IDS.iterator()) {
            mBottomNavigation!!.menu.removeItem(id)
        }
        // Verify selected ID is zero
        assertEquals(0, mBottomNavigation!!.selectedItemId)

        // Add an item
        mBottomNavigation!!.menu.add(0, R.id.item_home, 0, R.string.nav_button_home)
        // Verify item is selected
        assertEquals(R.id.item_home, mBottomNavigation!!.selectedItemId)

        // Try selecting an invalid ID
        mBottomNavigation!!.selectedItemId = R.id.item_empty_two
        // Verify the view has not changed
        assertEquals(R.id.item_home, mBottomNavigation!!.selectedItemId)
    }

    @UiThreadTest
    @Test
    @SmallTest
    @Throws(Throwable::class)
    fun testItemChecking() {
        val menu = mBottomNavigation!!.menu
        assertTrue(menu.getItem(0).isChecked)
        checkAndVerifyExclusiveItem(menu, R.id.item_home)
        checkAndVerifyExclusiveItem(menu, R.id.item_empty_one)
        checkAndVerifyExclusiveItem(menu, R.id.item_empty_two)
        checkAndVerifyExclusiveItem(menu, R.id.item_setting)
    }

    @Throws(Throwable::class)
    private fun checkAndVerifyExclusiveItem(menu: Menu, id: Int) {
        menu.findItem(id).isChecked = true
        for (i in 0 until menu.size()) {
            val item = menu.getItem(i)
            if (item.itemId == id) {
                assertTrue(item.isChecked)
            } else {
                assertFalse(item.isChecked)
            }
        }
    }

}