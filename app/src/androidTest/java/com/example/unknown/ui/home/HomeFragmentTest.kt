package com.example.unknown.ui.home

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.unknown.R
import com.example.unknown.RecyclerViewActions
import com.example.unknown.screen.main.MainActivity
import com.example.unknown.utils.EspressoIdlingResourceRule
import org.hamcrest.CoreMatchers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt

@RunWith(AndroidJUnit4::class)
class HomeFragmentTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @get: Rule
    val espressoIdlingResoureRule = EspressoIdlingResourceRule()


    @Test
    fun a_test_isListFragmentVisible_onAppLaunch() {
        onView(withId(R.id.rcvUserList)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        /*onView(withId(R.id.progress_bar)).check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))*/
    }
}