package com.example.unknown

import android.app.Application
import com.chibatching.kotpref.Kotpref
import com.example.unknown.koin.forecastModule
import com.example.unknown.koin.networkModule
import com.example.unknown.koin.prefModule
import com.example.unknown.koin.viewModelModule
import com.squareup.leakcanary.LeakCanary
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import timber.log.Timber

class UnknownApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        Kotpref.init(this)

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }

        LeakCanary.install(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidLogger()
            androidContext(this@UnknownApplication)
            modules(listOf(prefModule, networkModule, viewModelModule, forecastModule))
        }
    }

}