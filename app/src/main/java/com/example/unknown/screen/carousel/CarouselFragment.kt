package com.example.unknown.screen.carousel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.network.ErrorMessage
import com.example.network.response.UserResponse
import com.example.unknown.R
import com.example.unknown.ui.base.fragment.BaseFragment
import com.example.unknown.viewModel.main.MainViewModel
import kotlinx.android.synthetic.main.fragment_carousel.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class CarouselFragment : BaseFragment() {

    // Lazy injected Presenter instance
    private val viewModel: MainViewModel by viewModel()
    private var userList: MutableList<UserResponse> = mutableListOf()
    private lateinit var adapter: CarouselAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_carousel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.status.observe(viewLifecycleOwner, Observer { status ->

            when (status) {

                is MainViewModel.Status.Loading -> {
                    emptyView.showLoading()
                }

                is MainViewModel.Status.Success -> {
                    Timber.i("${status.viewDataBundle.userResponse}")
                    Timber.i("${status.viewDataBundle.ads}")

                    adapter.setList(status.viewDataBundle.userResponse)
                    view_pager.adapter = adapter

                    emptyView.showContent()
                }

                is MainViewModel.Status.Error -> {
                    /*Timber.i("${status.networkError.message}")*/
                    when (ErrorMessage.from(status.networkError.message ?: "")) {
                        ErrorMessage.TIME_OUT -> {
                            Timber.i("${status.networkError.message}")
                            emptyView.showNoConnection()
                        }
                        ErrorMessage.NOT_FOUND -> {
                            Timber.i("${status.networkError.message}")
                            emptyView.showEmpty()
                        }
                        else -> {
                            Timber.i("${status.networkError.message}")
                            emptyView.showUnKnowError()
                        }
                    }
                }
            }
        })

        viewModel.getAllUser()

        adapter =
            CarouselAdapter(requireActivity()) {
                Timber.i("You click position : $it")
            }
    }

    companion object {
        fun newInstance(): CarouselFragment {
            val args = Bundle()
            val fragment = CarouselFragment()
            fragment.arguments = args
            return fragment
        }
    }

}