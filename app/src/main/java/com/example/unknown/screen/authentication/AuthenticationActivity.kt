package com.example.unknown.screen.authentication

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.network.ResponseHandler
import com.example.network.Status
import com.example.unknown.R
import com.example.unknown.extension.setOnClickWithAnimationListener
import com.example.unknown.screen.PinCodeActivity
import com.example.unknown.utils.UserInfo
import com.example.unknown.viewModel.Authentication.AuthenticationViewModel
import com.microsoft.identity.client.IAccount
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.regex.Pattern


class AuthenticationActivity : AppCompatActivity() {

    private val viewModel: AuthenticationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initViewModelObserve()

        viewModel.context = this
        viewModel.checkUserStateLogin()

        btnSignIn.setOnClickWithAnimationListener {
            onButtonSignIn()
        }
    }

    private fun initViewModelObserve() {
        viewModel.status.observe(this, Observer { status ->

            when (status) {

                is AuthenticationViewModel.Status.Loading -> {

                }

                is AuthenticationViewModel.Status.SignInSuccess -> {
                    val intent = Intent(this, PinCodeActivity::class.java)
                    if (UserInfo.pinCode.isEmpty() || UserInfo.pinCode.isBlank()) {
                        intent.putExtra(PinCodeActivity.EXTRA_HAS_CREATE_PIN, true)
                    } else {
                        intent.putExtra(PinCodeActivity.EXTRA_PIN_CODE, UserInfo.pinCode)
                    }
                    startActivity(intent)
                }

                is AuthenticationViewModel.Status.UpdateUI -> {
                    updateUI(status.viewDataBundle.account)
                }

                is AuthenticationViewModel.Status.HaveUserLoginOnThisDevice -> {
                    viewModel.loadAccount()
                }

                is AuthenticationViewModel.Status.Error -> {
                    val error =
                        ResponseHandler().handleException<String>(status.networkError as Exception)
                    if (error.status == Status.ERROR) {
                        Timber.i("${error.message}")
                        emptyView.showUnKnowError()
                    }
                }
            }
        })
    }

    private fun onButtonSignIn() {
        viewModel.signIn()
    }

    /**
     * Updates UI when app sign out succeeds
     */
    private fun updateUI(account: IAccount?) {
        if (account != null) {
            neomorphFrameLayout3.visibility = View.GONE
            neomorphFrameLayout4.visibility = View.VISIBLE
            Timber.i("Account : ${account.username}")
        } else {
            neomorphFrameLayout3.visibility = View.VISIBLE
            neomorphFrameLayout4.visibility = View.GONE
        }
    }

}