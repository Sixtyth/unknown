package com.example.unknown.screen.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.unknown.screen.carousel.CarouselFragment
import com.example.unknown.ui.EmptyFragment
import com.example.unknown.ui.home.HomeFragment
import com.example.unknown.ui.setting.SettingFragment

class ButtonNavigationViewPager(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> HomeFragment.newInstance()
            1 -> CarouselFragment.newInstance()
            2 -> EmptyFragment.newInstance()
            3 -> SettingFragment.newInstance()
            else -> {
                throw IllegalStateException("can't find the fragment of position")
            }
        }
    }
    override fun getCount(): Int = 4
}