package com.example.unknown.screen.carousel

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.network.response.UserResponse
import com.example.unknown.R
import com.example.unknown.ui.base.AbstractPagerAdapter

class CarouselAdapter(
    internal var context: Context,
    var itemClick: (position: Int) -> Unit
) : AbstractPagerAdapter<UserResponse>() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        /* Inflate layout */
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.item_user_list, container, false)
        /* View */
        val avatarImage: ImageView by lazy { view.findViewById<ImageView>(R.id.ivAvatar) }
        val fullNameText: TextView by lazy { view.findViewById<TextView>(R.id.tvFirstName) }
        val emailText: TextView by lazy { view.findViewById<TextView>(R.id.tvEmail) }
        val rootView: ConstraintLayout by lazy { view.findViewById<ConstraintLayout>(R.id.rootView) }

        /* Set data */
        Glide.with(context)
            .load(items[position].avatar)
            .apply(RequestOptions().apply {
                this.centerCrop()
                this.error(ContextCompat.getDrawable(context, R.drawable.button_disable))
            })
            .into(avatarImage)

        fullNameText.text = context.getString(
            R.string.full_name,
            items[position].firstName,
            items[position].lastName
        )
        emailText.text = items[position].email

        rootView.setOnClickListener {
            itemClick.invoke(position)
        }

        container.addView(view)
        return view
    }

}