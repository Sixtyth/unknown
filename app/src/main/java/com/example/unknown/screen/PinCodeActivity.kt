package com.example.unknown.screen

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.unknown.R
import com.example.unknown.extension.setOnClickWithAnimationListener
import com.example.unknown.extension.toggle
import com.example.unknown.screen.main.MainActivity
import com.example.unknown.ui.base.activity.BaseActivity
import com.example.unknown.ui.views.PinEntryView
import com.example.unknown.utils.UserInfo
import kotlinx.android.synthetic.main.activity_pin_code.*
import timber.log.Timber

class PinCodeActivity : BaseActivity() {

    private var pinCode: String? = null
    private var storePinCode: String? = null
    private var hasCreatePinCode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin_code)

        intent.let {
            hasCreatePinCode = it.getBooleanExtra(EXTRA_HAS_CREATE_PIN, false)
            storePinCode = it.getStringExtra(EXTRA_PIN_CODE)
        }

        pinEntryView.setListener(
            object : PinEntryView.OnPinEnteredListener {
                override fun onPinEntered(pin: String?) {
                    Toast.makeText(this@PinCodeActivity, "Pin entered: $pin", Toast.LENGTH_LONG)
                        .show()
                }
            }
        )

        btnOne.setOnClickWithAnimationListener {
            getPinCode("1")
        }
        btnTwo.setOnClickWithAnimationListener {
            getPinCode("2")
        }
        btnThree.setOnClickWithAnimationListener {
            getPinCode("3")
        }
        btnFour.setOnClickWithAnimationListener {
            getPinCode("4")
        }
        btnFive.setOnClickWithAnimationListener {
            getPinCode("5")
        }
        btnSix.setOnClickWithAnimationListener {
            getPinCode("6")
        }
        btnSeven.setOnClickWithAnimationListener {
            getPinCode("7")
        }
        btnEight.setOnClickWithAnimationListener {
            getPinCode("8")
        }
        btnNine.setOnClickWithAnimationListener {
            getPinCode("9")
        }
        btnZero.setOnClickWithAnimationListener {
            getPinCode("0")
        }
        neomorphFrameLayoutRemove.setOnClickWithAnimationListener {
            if (pinCode != null) {
                removeLastPinCode(pinCode!!)
            }
        }

    }

    private fun getPinCode(pin: String) {
        if (pinCode == null) {
            pinCode = pin
        } else {
            pinCode += pin
        }

        if (pinCode!!.length == 4) {
            isValidHasCreatePinCode(hasCreatePinCode, storePinCode, pinCode)
            pinCode = ""
            pinEntryView.clearText()

        } else {
            pinEntryView.text = pinCode ?: ""
        }
    }

    private fun isValidPinCode(storePinCode: String, pinCode: String): Boolean {
        return if (hasCreatePinCode) {
            storePinCode == pinCode
        } else {
            storePinCode == pinCode
        }
    }

    private fun storeThePinCode(pinCode: String) {
        storePinCode = pinCode
    }

    private fun isValidHasCreatePinCode(
        hasCreate: Boolean,
        storePinCode: String?,
        pinCode: String?
    ) {
        if (hasCreate) {
            if (storePinCode != null) {
                if (isValidPinCode(storePinCode, pinCode!!)) {
                    UserInfo.pinCode = storePinCode
                    startActivity(Intent(this, MainActivity::class.java))
                } else {
                    Toast.makeText(
                        this,
                        "Pin code doesn't math please try again",
                        Toast.LENGTH_SHORT
                    ).show()
                    clearAllPinCode()
                }
            } else {
                storeThePinCode(pinCode!!)
            }
        } else {
            if (isValidPinCode(storePinCode!!, pinCode!!)) {
                UserInfo.pinCode = storePinCode
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                Toast.makeText(this, "Pin code doesn't math please try again", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun clearAllPinCode() {
        pinCode = ""
        storePinCode = ""
        pinEntryView.clearText()
    }

    private fun removeLastPinCode(pin: String) {
        val lastDataRemove: String? = removeLastCharacter(pin)
        pinCode = lastDataRemove
        Timber.i("Last Data Remove : $lastDataRemove")
        Timber.i("Last Data Remove : ${pinEntryView.text}")

        if (lastDataRemove != null) {
            pinEntryView.text = lastDataRemove
        }
    }

    private fun removeLastCharacter(str: String?): String? {
        var result: String? = null
        if (str != null && str.isNotEmpty()) {
            result = str.substring(0, str.length - 1)
        }
        return result
    }

    companion object {
        const val EXTRA_HAS_CREATE_PIN = "EXTRA_HAS_CREATE_PIN"
        const val EXTRA_PIN_CODE = "EXTRA_PIN_CODE"
    }

}