package com.example.unknown.screen.main

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.unknown.R
import com.example.unknown.ui.base.activity.BaseActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.microsoft.identity.client.*
import com.microsoft.identity.client.exception.MsalException
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nonSwipeableViewPager.apply {
            this.adapter = ButtonNavigationViewPager(
                supportFragmentManager
            )
        }

        btnNavView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    private val onNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.item_home -> {
                    // do this event
                    nonSwipeableViewPager.setCurrentItem(0, true)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.item_empty_one -> {
                    // do this event
                    nonSwipeableViewPager.setCurrentItem(1, true)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.item_empty_two -> {
                    // do this event
                    nonSwipeableViewPager.setCurrentItem(2, true)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.item_setting -> {
                    // do this event
                    nonSwipeableViewPager.setCurrentItem(3, true)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }
}