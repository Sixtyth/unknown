package com.example.unknown.utils

import com.chibatching.kotpref.KotprefModel
import java.util.*

object UserInfo : KotprefModel() {
    var token by stringPref()
    var pinCode by stringPref()
    var languageLocale by stringPref("th")
}