package com.example.unknown.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import java.util.*

object ConfigurationWrapper {

    //Creates a Context with updated Configuration.
    private fun wrapConfiguration(context: Context, config: Configuration, locale: Locale): Context {
        val overrideConfiguration = context.resources.configuration
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale)
            context.createConfigurationContext(config)
        } else {
            overrideConfiguration.locale = locale
            context.resources.updateConfiguration(overrideConfiguration, context.resources.displayMetrics)
            context
        }
    }

    // Creates a Context with updated Locale.
    fun wrapLocale(context: Context, locale: Locale): Context {
        val res = context.resources
        val config = res.configuration
        return wrapConfiguration(context, config, locale)
    }
}