package com.example.unknown.koin

import com.example.unknown.persistence.Preferences
import com.example.unknown.service.*
import com.example.unknown.viewModel.main.MainViewModel
import com.example.network.repository.UnknownRepository
import com.example.unknown.viewModel.Authentication.AuthenticationViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.koin.core.module.Module

private const val NORMAL_OK_HTTP = "NORMAL_OK_HTTP"
private const val AUTHENTICATED_OK_HTTP = "AUTHENTICATED_OK_HTTP"

val prefModule = module {
    single { Preferences(androidContext()) }
}

val networkModule: Module = module {
    factory { provideLoggingInterceptor() }
    single(named(NORMAL_OK_HTTP)) { provideOkHttpClient() }
    single(named(AUTHENTICATED_OK_HTTP)) { provideAuthenticatedOkHttpClient() }
    single(named(NORMAL_OK_HTTP)) { provideRetrofit(get(named(NORMAL_OK_HTTP)), get()) }
    single(named(AUTHENTICATED_OK_HTTP)) {
        provideAuthenticatedRetrofit(
            get(named(AUTHENTICATED_OK_HTTP)),
            get()
        )
    }
    single { provideGson() }
}

val viewModelModule = module {
    factory { MainViewModel(get()) }
    factory { AuthenticationViewModel(get()) }
}

val forecastModule = module {
    factory { UnknownRepository(get(named(NORMAL_OK_HTTP))) }
}