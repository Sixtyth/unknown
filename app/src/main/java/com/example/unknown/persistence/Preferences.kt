package com.example.unknown.persistence

import android.content.Context
import android.content.SharedPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

class Preferences(context: Context) {

    private val preferences: SharedPreferences =
        context.getSharedPreferences("prefs", Context.MODE_PRIVATE)
    private val shouldLoginKey = "fragmentContent"

    init {
        storeShouldLogin(false)
    }

    private fun storeShouldLogin(content: Boolean) {
        preferences.edit().putBoolean(shouldLoginKey, content).apply()
    }

    fun getShouldLogin(): Boolean? {
        return preferences.getBoolean(shouldLoginKey, false)
    }


}