package com.example.unknown.ui.base.sectionviewadapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import kotlin.reflect.KClass

open class SectionRecyclerViewAdapter(var mode: SectionViewMode = SectionViewMode.STANDARD) : AbstractRecyclerViewAdapter<Any, RecyclerView.ViewHolder>() {

    interface SectionViewAdapter<in T : Any, H : RecyclerView.ViewHolder> {

        fun onCreateViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup?, viewType: Int): H

        fun onBindViewHolder(holder: H, item: T, position: Int)
    }

    interface SectionListViewAdapter<in T : Any, H : RecyclerView.ViewHolder> {

        fun onCreateViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup?, viewType: Int): H

        fun onBindViewHolder(holder: H, item: List<T>, position: Int)
    }

    private val sectionViewAdapters = mutableMapOf<Int, SectionViewAdapter<Any, RecyclerView.ViewHolder>>()

    private val sectionListViewAdapters = mutableMapOf<Int, SectionListViewAdapter<Any, RecyclerView.ViewHolder>>()

    private fun <T : Any> addSectionViewAdapter(type: Class<T>, sectionViewAdapter: SectionViewAdapter<T, out RecyclerView.ViewHolder>) =
            sectionViewAdapters.put(getItemViewTypeForClass(type), sectionViewAdapter as SectionViewAdapter<Any, RecyclerView.ViewHolder>)

    private fun <T : Any> addSectionListViewAdapter(type: Class<T>, sectionViewAdapter: SectionListViewAdapter<T, out RecyclerView.ViewHolder>) =
            sectionListViewAdapters.put(getItemViewTypeForClass(type), sectionViewAdapter as SectionListViewAdapter<Any, RecyclerView.ViewHolder>)

    fun <T : Any> addSectionViewAdapter(type: KClass<T>, sectionViewAdapter: SectionViewAdapter<T, out RecyclerView.ViewHolder>) =
            addSectionViewAdapter(type.java, sectionViewAdapter)

    fun <T : Any> addSectionListViewAdapter(type: KClass<T>, sectionViewAdapter: SectionListViewAdapter<T, out RecyclerView.ViewHolder>) =
            addSectionListViewAdapter(type.java, sectionViewAdapter)

    fun isItemOfType(position: Int, type: Class<Any>): Boolean = getItemViewType(position) == getItemViewTypeForClass(type)

    inline fun <reified T : Any> addSectionViewAdapter(sectionViewAdapter: SectionViewAdapter<T, out RecyclerView.ViewHolder>) =
            addSectionViewAdapter(T::class, sectionViewAdapter)

    inline fun <reified T : Any> addSectionListViewAdapter(sectionViewAdapter: SectionListViewAdapter<T, out RecyclerView.ViewHolder>) =
            addSectionListViewAdapter(T::class, sectionViewAdapter)

    override fun getItemViewType(position: Int) = getItemViewTypeForClass(getItem(position).javaClass)

    private fun getItemViewTypeForClass(type: Class<*>) = type.hashCode()

    private fun getSectionViewAdapter(viewType: Int) = sectionViewAdapters[viewType]!!

    private fun getSectionListViewAdapter(viewType: Int) = sectionListViewAdapters[viewType]!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return  if (mode == SectionViewMode.STANDARD) {
            getSectionViewAdapter(viewType).onCreateViewHolder(LayoutInflater.from(parent.context), parent, viewType)
        } else {
            getSectionListViewAdapter(viewType).onCreateViewHolder(LayoutInflater.from(parent.context), parent, viewType)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = getItem(position)

        if (mode == SectionViewMode.STANDARD) {
            val sectionViewAdapter = getSectionViewAdapter(getItemViewType(position))
            sectionViewAdapter.onBindViewHolder(holder, item, position)
        } else {
            val sectionViewAdapter = getSectionListViewAdapter(getItemViewType(position))
            sectionViewAdapter.onBindViewHolder(holder, listOf(item), position)
        }

    }
}

enum class SectionViewMode {
    STANDARD,
    LIST
}