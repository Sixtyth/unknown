package com.example.unknown.ui.base

import androidx.viewpager.widget.PagerAdapter

abstract class AbstractPagerAdapter<T>: PagerAdapter() {

    protected val items = ArrayList<T>()

    open fun setList(list: List<T>) {
        items.clear()
        addToList(list)
    }

    open fun addToList(list: List<T>) {
        items.addAll(list)
        notifyDataSetChanged()
    }

    override fun getCount(): Int = items.size

}