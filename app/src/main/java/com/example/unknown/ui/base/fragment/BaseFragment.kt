package com.example.unknown.ui.base.fragment

import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.view.children
import androidx.fragment.app.Fragment
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import com.example.unknown.R
import com.example.unknown.extension.hideKeyboard
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseFragment : Fragment() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    protected fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun onStop() {
        disposables.dispose()
        super.onStop()
    }

    protected val loadDialog: MaterialDialog by lazy {
        MaterialDialog.Builder(requireContext())
                .content(R.string.loading)
                .progress(true, 100)
                .cancelable(false)
                .autoDismiss(true)
                .build()
    }

    fun showDialogMessage(title: String, message: String, onPositive: (() -> Unit)? = null, onNegative: (() -> Unit)? = null) {
        val builder = MaterialDialog.Builder(requireContext())
                .title(title)
                .content(message)
        if (onPositive != null) {
            builder.positiveText(R.string.agree)
                    .onPositive { _, _ ->
                        onPositive.invoke()
                    }
        }
        if (onNegative != null) {
            builder.negativeText(R.string.cancel)
                    .onNegative { _, _ ->
                        onNegative.invoke()
                    }
        }
        builder.theme(Theme.LIGHT)
                .show()
    }


    fun setupUI(view: View) {
        if (view !is EditText) {
            view.setOnTouchListener { _, _ ->
                hideKeyboard()
                false
            }
        }

        if (view is ViewGroup) {
            for (child in view.children) {
                setupUI(child)
            }
        }
    }

}