package com.example.unknown.ui.base.sectionviewadapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class StaticViewAdapter<T : Any> :
    SectionRecyclerViewAdapter.SectionViewAdapter<T, RecyclerView.ViewHolder> {

    protected abstract val layoutId: Int

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: T, position: Int) {}

    override fun onCreateViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup?, viewType: Int) = ViewHolder(layoutInflater.inflate(layoutId, parent, false))

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}