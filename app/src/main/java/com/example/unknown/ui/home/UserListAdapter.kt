package com.example.unknown.ui.home

import android.content.Context
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.network.response.UserResponse
import com.example.unknown.R
import com.example.unknown.ui.base.sectionviewadapter.SectionRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_user_list.view.*


class UserListAdapter(val context: Context, val itemClick: (position: Int) -> Unit) :
    SectionRecyclerViewAdapter.SectionViewAdapter<UserResponse, UserListAdapter.UserListHolder> {

    private var lastPosition = -1

    override fun onCreateViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup?,
        viewType: Int
    ): UserListHolder =
        UserListHolder(layoutInflater.inflate(R.layout.item_user_list, parent, false))

    override fun onBindViewHolder(
        holder: UserListHolder,
        item: UserResponse,
        position: Int
    ) {
        holder.itemView.tvFirstName.text =
            context.getString(R.string.full_name, item.firstName, item.lastName)

        holder.itemView.tvEmail.text = item.email

        Glide.with(context)
            .load(item.avatar)
            .apply(RequestOptions().apply {
                this.centerCrop()
                this.error(ContextCompat.getDrawable(context, R.drawable.button_disable))
            })
            .into(holder.itemView.ivAvatar)

        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(
                context,
                if (position > lastPosition) R.anim.up_from_bottom else R.anim.down_from_top
            )
            AsyncTask.execute {
                holder.itemView.startAnimation(animation)
            }
            lastPosition = position
        }
    }

    class UserListHolder(view: View) : RecyclerView.ViewHolder(view)

}