package com.example.unknown.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.network.ErrorCodes
import com.example.network.ErrorMessage
import com.example.network.ResponseHandler
import com.example.network.Status
import com.example.network.response.DownStreamEntity
import com.example.network.response.UserResponse
import com.example.unknown.R
import com.example.unknown.ui.base.sectionviewadapter.LoadMoreViewAdapter
import com.example.unknown.ui.base.sectionviewadapter.SectionRecyclerViewAdapter
import com.example.unknown.viewModel.main.MainViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.HttpException
import timber.log.Timber

class HomeFragment : Fragment() {

    // Lazy injected Presenter instance
    private val viewModel: MainViewModel by viewModel()
    private val adapter get() = rcvUserList.adapter as LoadMoreViewAdapter
    private var userList: MutableList<UserResponse> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.status.observe(viewLifecycleOwner, Observer { status ->

            when (status) {

                is MainViewModel.Status.Loading -> {
                    emptyView.showLoading()
                }

                is MainViewModel.Status.Success -> {
                    Timber.i("${status.viewDataBundle.userResponse}")
                    Timber.i("${status.viewDataBundle.ads}")
                    adapter.isNextItemAvailable = status.viewDataBundle.userResponse.isNotEmpty()
                    status.viewDataBundle.userResponse.forEach {
                        userList.add(it)
                    }
                    adapter.setList(userList)
                    emptyView.showContent()
                }

                is MainViewModel.Status.Error -> {
                    /*Timber.i("${status.networkError.message}")*/
                    when (ErrorMessage.from(status.networkError.message ?: "")) {
                        ErrorMessage.TIME_OUT -> {
                            Timber.i("${status.networkError.message}")
                            emptyView.showNoConnection()
                        }
                        ErrorMessage.NOT_FOUND -> {
                            Timber.i("${status.networkError.message}")
                            emptyView.showEmpty()
                        }
                        else -> {
                            Timber.i("${status.networkError.message}")
                            emptyView.showUnKnowError()
                        }
                    }
                }
            }
        })

        rcvUserList.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )

        rcvUserList.adapter = LoadMoreViewAdapter().apply {
            addSectionViewAdapter(UserListAdapter(requireActivity()) { position ->

            })
        }

        adapter.onLoadMore = {
            if (adapter.isNextItemAvailable) {
                viewModel.getAllUser(adapter.currentPage)
            }
        }

        viewModel.getAllUser()
        userList.clear()
        rcvUserList.scrollToPosition(0)
        adapter.currentPage = 0
    }

    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }
    }
}