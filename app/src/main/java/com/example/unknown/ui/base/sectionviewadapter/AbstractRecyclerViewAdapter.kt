package com.example.unknown.ui.base.sectionviewadapter

import androidx.recyclerview.widget.RecyclerView

abstract class AbstractRecyclerViewAdapter<T, H : RecyclerView.ViewHolder> : RecyclerView.Adapter<H>() {

    protected val items = ArrayList<T>()

    protected fun getItem(position: Int): T = items[position]

    fun setListAnimated(list: List<T>) {
        items.clear()
        notifyDataSetChanged()
        items.addAll(list)
        notifyItemRangeInserted(0, list.size)
    }

    fun clearListAnimated() {
        notifyItemRangeRemoved(0, itemCount)
        items.clear()
    }

    open fun setList(list: List<T>) {
        items.clear()
        addToList(list)
    }

    open fun addToList(list: List<T>) {
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun addToList(item: T) {
        items.add(item)
        notifyItemInserted(items.size - 1)
    }

    open fun removeAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun getItemCount(): Int = items.size

}