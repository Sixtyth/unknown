package com.example.unknown.ui.views

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import com.example.unknown.R

class EmptyView : RelativeLayout {

    // Empty
    private var onEmptyClickListener: (() -> Unit)? = null

    // Error
    private var onClickListener: (() -> Unit)? = null

    // No connection
    private var onFlatClickListener: (() -> Unit)? = null

    fun setOnClickListener(listener: () -> Unit) {
        onClickListener = listener
    }

    fun setOnEmptyClickListener(listener: () -> Unit) {
        onEmptyClickListener = listener
    }

    fun setOnReloadClickListener(listener: () -> Unit) {
        onFlatClickListener = listener
    }

    private val mEmptyContainer: LinearLayout by lazy {
        rootView.findViewById<LinearLayout>(
            R.id.llEmpty
        )
    }

    private val mIcon: ImageView by lazy { findViewById<ImageView>(R.id.ivImage) }
    private val mTitle: TextView by lazy { findViewById<TextView>(R.id.tvTitle) }
    private val mMessage: TextView by lazy { findViewById<TextView>(R.id.tvMessage) }
    private val mButtonNormal: Button by lazy { findViewById<Button>(R.id.btnAction) }
    private val mButtonFlat: Button by lazy { findViewById<Button>(R.id.btnFlat) }
    private val mLoadingIndicator: ProgressBar by lazy { rootView.findViewById<ProgressBar>(R.id.loadingIndicator) }

    // Empty
    private var mEmptyIcon: Drawable? = null
    private var mEmptyTitle: String? = null
    private var mEmptyMessage: String? = null
    private var mEmptyButtonText: String? = null
    private var mEmptyBackgroundColor = -1
    private var mEmptyTitleColor = -1
    private var mEmptyMessageColor = -1

    // No connection
    private var mNoConnectionIcon: Drawable? =
        ContextCompat.getDrawable(context, R.drawable.ic_cloud_off_black_24dp)
    private var mNoConnectionTitle: String? = null
    private var mNoConnectionMessage: String? = context!!.getString(R.string.no_connection)
    private var mNoConnectionButtonText: String? = context!!.getString(R.string.retry)
    private var mNoConnectionBackgroundColor = -1
    private var mNoConnectionTitleColor = -1
    private var mNoConnectionMessageColor = -1

    // Error
    private var mErrorIcon: Drawable? = null
    private var mErrorTitle: String? = context!!.getString(R.string.error_default)
    private var mErrorMessage: String? = context!!.getString(R.string.something_went_wrong)
    private var mErrorButtonText: String? = null
    private var mErrorBackgroundColor = -1
    private var mErrorTitleColor = -1
    private var mErrorMessageColor = -1

    private var mChildViews = ArrayList<View>()

    private var indicatorColor = ContextCompat.getColor(context!!, R.color.black)

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs!!)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs!!)
    }

    private fun init(attrs: AttributeSet) {
        val a = context!!.obtainStyledAttributes(attrs, R.styleable.EmptyView)
        try {
            if (a.hasValue(R.styleable.EmptyView_emptyIconDrawable)) {
                mEmptyIcon = ContextCompat.getDrawable(
                    context,
                    a.getResourceId(R.styleable.EmptyView_emptyIconDrawable, -1)
                )
            }
            mEmptyTitle = a.getString(R.styleable.EmptyView_emptyTitle)
            mEmptyMessage = a.getString(R.styleable.EmptyView_emptyMessage)
            mEmptyButtonText = a.getString(R.styleable.EmptyView_emptyButtonText)
            mEmptyBackgroundColor = a.getColor(
                R.styleable.EmptyView_emptyBackgroundColor,
                ContextCompat.getColor(context!!, R.color.white)
            )
            mEmptyTitleColor = a.getColor(
                R.styleable.EmptyView_emptyTitleColor,
                ContextCompat.getColor(context!!, R.color.dark_charcoal)
            )
            mEmptyMessageColor = a.getColor(
                R.styleable.EmptyView_emptyMessageColor,
                ContextCompat.getColor(context!!, R.color.dark_charcoal)
            )


            if (a.hasValue(R.styleable.EmptyView_noConnectionIconDrawable)) {
                mNoConnectionIcon = ContextCompat.getDrawable(
                    context,
                    a.getResourceId(
                        R.styleable.EmptyView_noConnectionIconDrawable,
                        R.drawable.ic_cloud_off_black_24dp
                    )
                )
            }
            mNoConnectionTitle = a.getString(R.styleable.EmptyView_noConnectionTitle)
            if (a.getString(R.styleable.EmptyView_noConnectionMessage) != null) {
                mNoConnectionMessage = a.getString(R.styleable.EmptyView_noConnectionMessage)
            }
            if (a.getString(R.styleable.EmptyView_noConnectionButtonText) != null) {
                mNoConnectionButtonText = a.getString(R.styleable.EmptyView_noConnectionButtonText)
            }
            mNoConnectionBackgroundColor = a.getColor(
                R.styleable.EmptyView_noConnectionBackgroundColor,
                ContextCompat.getColor(context!!, R.color.white)
            )
            mNoConnectionTitleColor = a.getColor(
                R.styleable.EmptyView_noConnectionTitleColor,
                ContextCompat.getColor(context!!, R.color.dark_charcoal)
            )
            mNoConnectionMessageColor = a.getColor(
                R.styleable.EmptyView_noConnectionMessageColor,
                ContextCompat.getColor(context!!, R.color.dark_charcoal)
            )

//            mErrorIcon = ContextCompat.getDrawable(context, a.getResourceId(R.styleable.EmptyView_errorIconDrawable,-1))
            if (a.getString(R.styleable.EmptyView_errorTitle) != null) {
                mErrorTitle = a.getString(R.styleable.EmptyView_errorTitle)
            }
            if (a.getString(R.styleable.EmptyView_errorMessage) != null) {
                mErrorMessage = a.getString(R.styleable.EmptyView_errorMessage)
            }
            mErrorButtonText = a.getString(R.styleable.EmptyView_errorButtonText)
            mErrorBackgroundColor = a.getColor(
                R.styleable.EmptyView_errorBackgroundColor,
                ContextCompat.getColor(context!!, R.color.white)
            )
            mErrorTitleColor = a.getColor(
                R.styleable.EmptyView_errorTitleColor,
                ContextCompat.getColor(context!!, R.color.dark_charcoal)
            )
            mErrorMessageColor = a.getColor(
                R.styleable.EmptyView_errorMessageColor,
                ContextCompat.getColor(context!!, R.color.dark_charcoal)
            )

            indicatorColor = a.getColor(
                R.styleable.EmptyView_indicatorColor,
                ContextCompat.getColor(context!!, R.color.black)
            )
        } finally {
            a.recycle()
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        inflate(context, R.layout.empty_view, this)
        mLoadingIndicator.tag = TAG
        mEmptyContainer.tag = TAG

        mButtonFlat.setOnClickListener {
            onFlatClickListener!!.invoke()
        }

        state = State.CONTENT
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        super.addView(child, index, params)
        if (child!!.visibility == View.VISIBLE && (child.tag == null || child.tag!! != TAG)) {
            mChildViews.add(child)
        }
    }

    var state: State = State.LOADING
        set(value) {
            when (value) {
                State.LOADING -> {
                    mLoadingIndicator.visibility = View.VISIBLE
                    mEmptyContainer.visibility = View.GONE
                    setChildVisibility(View.GONE)
                }
                State.EMPTY -> {
                    mLoadingIndicator.visibility = View.GONE
                    mEmptyContainer.visibility = View.VISIBLE
                    setUpEmptyView()
                    setChildVisibility(View.GONE)
                }
                State.CONTENT -> {
                    mLoadingIndicator.visibility = View.GONE
                    mEmptyContainer.visibility = View.GONE
                    setChildVisibility(View.VISIBLE)
                }
                State.NO_CONNECTION -> {
                    mLoadingIndicator.visibility = View.GONE
                    mEmptyContainer.visibility = View.VISIBLE
                    setUpNoConnection()
                    setChildVisibility(View.GONE)
                }
                State.ERROR -> {
                    mLoadingIndicator.visibility = View.GONE
                    mEmptyContainer.visibility = View.VISIBLE
                    setUpError()
                    setChildVisibility(View.GONE)
                }
            }
            field = value
        }

    private fun setUpEmptyView() {
        if (mEmptyIcon != null) {
            mIcon.setImageDrawable(mEmptyIcon)
            mIcon.visibility = View.VISIBLE
        } else {
            mIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_error_404_128dp))
            mIcon.visibility = View.VISIBLE
        }
        if (mEmptyTitle != null) {
            mTitle.visibility = View.VISIBLE
            mTitle.text = mEmptyTitle
        } else {
            mTitle.visibility = View.GONE
        }
        if (mEmptyMessage != null) {
            mMessage.visibility = View.VISIBLE
            mMessage.text = mEmptyMessage
        } else {
            mMessage.visibility = View.GONE
        }
        if (mEmptyButtonText != null) {
            mButtonNormal.visibility = View.VISIBLE
            mButtonNormal.text = mEmptyButtonText
            mButtonFlat.visibility = View.GONE
        } else {
            mButtonNormal.visibility = View.GONE
            mButtonFlat.visibility = View.GONE
        }
        mEmptyContainer.setBackgroundColor(mEmptyBackgroundColor)
        mButtonNormal.setOnClickListener {
            onEmptyClickListener?.invoke()
        }
    }

    private fun setUpError() {
        if (mErrorIcon != null) {
            mIcon.setImageDrawable(mErrorIcon)
            mIcon.visibility = View.VISIBLE
        } else {
            mIcon.visibility = View.GONE
        }
        if (mErrorTitle != null) {
            mTitle.setTextColor(mErrorTitleColor)
            mTitle.text = mErrorTitle
            mTitle.visibility = View.VISIBLE
        } else {
            mTitle.visibility = View.GONE
        }
        if (mErrorMessage != null) {
            mMessage.setTextColor(mErrorMessageColor)
            mMessage.text = mErrorMessage
            mMessage.visibility = View.VISIBLE
        } else {
            mMessage.visibility = View.GONE
        }
        if (mErrorButtonText != null) {
            mButtonNormal.visibility = View.VISIBLE
            mButtonNormal.text = mErrorButtonText
            mButtonFlat.visibility = View.GONE
        } else {
            mButtonNormal.visibility = View.GONE
            mButtonFlat.visibility = View.GONE
        }
        mEmptyContainer.setBackgroundColor(mErrorBackgroundColor)
        mButtonNormal.setOnClickListener {
            onClickListener?.invoke()
        }
    }

    private fun setUpNoConnection() {
        if (mNoConnectionIcon != null) {
            mIcon.setImageDrawable(mNoConnectionIcon)
            mIcon.visibility = View.VISIBLE
        } else {
            mIcon.visibility = View.GONE
        }
        if (mNoConnectionTitle != null) {
            mTitle.setTextColor(mNoConnectionTitleColor)
            mTitle.text = mNoConnectionTitle
            mTitle.visibility = View.VISIBLE
        } else {
            mTitle.visibility = View.GONE
        }
        if (mNoConnectionMessage != null) {
            mMessage.setTextColor(mNoConnectionMessageColor)
            mMessage.text = mNoConnectionMessage
            mMessage.visibility = View.VISIBLE
        } else {
            mMessage.visibility = View.GONE
        }
        if (mNoConnectionButtonText != null) {
            mButtonFlat.visibility = View.VISIBLE
            mButtonFlat.text = mNoConnectionButtonText
            mButtonNormal.visibility = View.GONE
        } else {
            mButtonFlat.visibility = View.GONE
            mButtonNormal.visibility = View.GONE
        }
        mEmptyContainer.setBackgroundColor(mNoConnectionBackgroundColor)
        mButtonNormal.setOnClickListener {
            onClickListener?.invoke()
        }
    }

    private fun setChildVisibility(visibility: Int) {
        for (view in mChildViews) {
            view.visibility = visibility
        }
    }

    fun showNoConnection() {
        state = State.NO_CONNECTION
    }

    fun showError(message: String) {
        mErrorMessage = message
        state = State.ERROR
    }

    fun showUnKnowError() {
        state = State.ERROR
    }

    fun showEmpty() {
        state = State.EMPTY
    }

    fun showContent() {
        state = State.CONTENT
    }

    fun showLoading() {
        state = State.LOADING
    }

    enum class State {
        LOADING, EMPTY, CONTENT, NO_CONNECTION, ERROR
    }

    companion object {
        const val TAG = "EmptyView"
    }


}