package com.example.unknown.ui.base.sectionviewadapter

class LoadMoreViewAdapter : SectionRecyclerViewAdapter() {

    var isNextItemAvailable = false
    var currentPage = 1
    var isError = false
    var onReloadClick: (() -> Unit)? = null
    var onLoadMore: (() -> Unit)? = null

    init {
        addSectionViewAdapter(LoadingViewAdapter {
            onLoadMore?.invoke()
        })
        addSectionViewAdapter(NoConnectionViewAdapter())
    }

    override fun setList(list: List<Any>) {
        super.setList(list)
        if (isNextItemAvailable) {
            currentPage++
            if (isError) {
                addToList(NoConnectionViewModel {
                    onReloadClick?.invoke()
                })
            } else {
                addToList(LoadingViewModel())
            }
        }
    }
}