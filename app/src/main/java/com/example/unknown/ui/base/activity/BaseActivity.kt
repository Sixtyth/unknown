package com.example.unknown.ui.base.activity

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.net.toUri
import com.example.unknown.utils.ConfigurationWrapper
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import com.example.unknown.R
import com.example.unknown.utils.UserInfo
import java.util.*

abstract class BaseActivity : AppCompatActivity() {


    var locale = UserInfo.languageLocale

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    protected val loadingDialog: MaterialDialog by lazy {
        MaterialDialog.Builder(this)
            .content(R.string.loading)
            .progress(true, 100)
            .cancelable(false)
            .build()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item!!)
    }

    fun openChromeTab(url: String) {
        val builder = CustomTabsIntent.Builder()
        builder.setShowTitle(true)

        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(this, url.toUri())
    }

    override fun attachBaseContext(newBase: Context?) {
        val locale = Locale(UserInfo.languageLocale)
        Locale.setDefault(locale)
        super.attachBaseContext(ConfigurationWrapper.wrapLocale(newBase!!, locale))
    }

    override fun applyOverrideConfiguration(overrideConfiguration: Configuration?) {
        if (overrideConfiguration != null) {
            val uiMode = overrideConfiguration.uiMode
            overrideConfiguration.setTo(baseContext.resources.configuration)
            overrideConfiguration.uiMode = uiMode
        }
        super.applyOverrideConfiguration(overrideConfiguration)
    }

    fun getCurrentLocale(): Locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        resources.configuration.locales[0]
    } else {
        // TODO API level 27
        resources.configuration.locale
    }

    fun showDialogMessage(
        title: String? = null,
        message: String,
        onPositive: (() -> Unit)? = null,
        onNegative: (() -> Unit)? = null
    ) {
        val builder = MaterialDialog.Builder(this)
        title?.let {
            builder.title(it)
        }
        builder.content(message)
        if (onPositive != null) {
            builder.positiveText(R.string.agree)
                .onPositive { _, _ ->
                    onPositive.invoke()
                }
        }
        if (onNegative != null) {
            builder.negativeText(R.string.cancel)
                .onNegative { _, _ ->
                    onNegative.invoke()
                }
        }
        builder.theme(Theme.LIGHT)
            .show()
    }

    override fun onResume() {
        super.onResume()
        if (locale != UserInfo.languageLocale) {
            recreate()
        }
    }
}