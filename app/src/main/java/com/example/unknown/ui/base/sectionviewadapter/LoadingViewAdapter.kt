package com.example.unknown.ui.base.sectionviewadapter

import androidx.recyclerview.widget.RecyclerView
import com.example.unknown.R

class LoadingViewAdapter(private val onLoadMore: () -> Unit) : StaticViewAdapter<LoadingViewModel>() {
    override val layoutId: Int = R.layout.item_loading

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: LoadingViewModel, position: Int) {
        onLoadMore()
    }
}

class LoadingViewModel