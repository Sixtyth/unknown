package com.example.unknown.ui.base.sectionviewadapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.unknown.R
import kotlinx.android.synthetic.main.item_car_no_connection.view.*

class NoConnectionViewAdapter : SectionRecyclerViewAdapter.SectionViewAdapter<NoConnectionViewModel, NoConnectionViewAdapter.ViewHolder> {
    override fun onBindViewHolder(holder: ViewHolder, item: NoConnectionViewModel, position: Int) {
        holder.itemView.tvRetry.setOnClickListener {
            item.onItemClick()
        }
    }

    override fun onCreateViewHolder(layoutInflater: LayoutInflater, parent: ViewGroup?, viewType: Int): ViewHolder = ViewHolder(layoutInflater.inflate(
        R.layout.item_car_no_connection, parent, false))

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}

class NoConnectionViewModel(val onItemClick: () -> Unit)