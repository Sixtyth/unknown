package com.example.unknown.viewModel.Authentication

import android.app.Activity
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.network.repository.UnknownRepository
import com.example.unknown.R
import com.example.unknown.utils.UserInfo
import com.example.unknown.viewModel.DisposableViewModel
import com.microsoft.identity.client.*
import com.microsoft.identity.client.ISingleAccountPublicClientApplication.SignOutCallback
import com.microsoft.identity.client.exception.MsalException
import timber.log.Timber

class AuthenticationViewModel(private val repo: UnknownRepository) : DisposableViewModel() {

    private val mutableStatus = MutableLiveData<Status>()

    val status: LiveData<Status> = mutableStatus

    /* Azure AD Variables */
    private var mSingleAccountApp: ISingleAccountPublicClientApplication? = null
    private var mAccount: IAccount? = null
    private val SCOPES = arrayOf("Files.Read")

    /* Azure AD v2 Configs */
    private val AUTHORITY = "https://login.microsoftonline.com/d10bb718-8905-46de-b6d4-32c7600ca055"

    var context: Context? = null

    private val currentValue: Status
        get() = mutableStatus.value!!

    init {
        mutableStatus.value =
            Status.Loading(
                ViewDataBundle()
            )
    }

    fun signIn() {
        mSingleAccountApp!!.signIn(context as Activity, "", SCOPES, getAuthInteractiveCallback()!!)
    }

    fun signOut() {
        mSingleAccountApp!!.signOut(object : SignOutCallback {
            override fun onSignOut() {
                UserInfo.token = ""
                mutableStatus.value = Status.UpdateUI(ViewDataBundle(null))
            }

            override fun onError(exception: MsalException) {
                mutableStatus.value =
                    Status.Error(
                        exception as Throwable,
                        currentValue.viewDataBundle
                    )
            }
        })
    }

    fun checkUserStateLogin() {
        // Creates a PublicClientApplication object with res/raw/auth_config_single_account.json
        PublicClientApplication.createSingleAccountPublicClientApplication(context!!,
            R.raw.auth_config_single_account,
            object : IPublicClientApplication.ISingleAccountApplicationCreatedListener {
                override fun onCreated(application: ISingleAccountPublicClientApplication?) {
                    /**
                     * This test app assumes that the app is only going to support one account.
                     * This requires "account_mode" : "SINGLE" in the config json file.
                     */
                    mSingleAccountApp = application
                    Timber.i("${(application != null)}")
                    mutableStatus.value = Status.HaveUserLoginOnThisDevice(ViewDataBundle())
                }

                override fun onError(exception: MsalException?) {
                    Timber.i("Public Client On Error : $exception")
                    mutableStatus.value =
                        Status.Error(
                            exception as Throwable,
                            currentValue.viewDataBundle
                        )
                }

            })
    }

    fun loadAccount() {
        if (mSingleAccountApp == null) {
            return
        }
        mSingleAccountApp!!.getCurrentAccountAsync(object :
            ISingleAccountPublicClientApplication.CurrentAccountCallback {
            override fun onAccountLoaded(activeAccount: IAccount?) {
                // You can use the account data to update your UI or your app database.
                if (activeAccount == null) {
                    mAccount = activeAccount
                    mutableStatus.value = Status.UpdateUI(ViewDataBundle(activeAccount))
                } else {
                    mSingleAccountApp!!.acquireTokenSilentAsync(
                        SCOPES, AUTHORITY, getAuthSilentCallback()!!
                    )
                }
            }

            override fun onAccountChanged(
                priorAccount: IAccount?,
                currentAccount: IAccount?
            ) {
                Timber.i(priorAccount?.username ?: "Unknown")
                Timber.i(currentAccount?.username ?: "Unknown")

                if (currentAccount == null) {
                    // Perform a cleanup task as the signed-in account changed.
                    /*showToastOnSignOut()*/
                }
            }

            override fun onError(exception: MsalException) {
                Timber.i("onError $exception")
            }
        })
    }

    private fun getAuthInteractiveCallback(): AuthenticationCallback? {
        return object : AuthenticationCallback {
            override fun onSuccess(authenticationResult: IAuthenticationResult) {
                /*Successfully got a token, use it to call a protected resource - MSGraph*/
                UserInfo.token = authenticationResult.accessToken
                /* Update UI */
                mutableStatus.value = Status.SignInSuccess(ViewDataBundle(authenticationResult.account))
            }

            override fun onError(exception: MsalException) {
                /* Failed to acquireToken */
                exception.stackTrace.forEach {
                    Timber.i("Error : $it")
                }
            }

            override fun onCancel() {
                /* User canceled the authentication */
                Timber.i("User cancelled login.")
            }
        }
    }

    private fun getAuthSilentCallback(): SilentAuthenticationCallback? {
        return object : SilentAuthenticationCallback {
            override fun onSuccess(authenticationResult: IAuthenticationResult) {
                Timber.i("Successfully authenticated")
                /*Successfully got a token, use it to call a protected resource - MSGraph*/
                UserInfo.token = authenticationResult.accessToken
                /* Update UI */
                mutableStatus.value = Status.SignInSuccess(ViewDataBundle(authenticationResult.account))
            }

            override fun onError(exception: MsalException) {
                Timber.i("Authentication failed: $exception")
                mutableStatus.value = Status.UpdateUI(ViewDataBundle(null))
            }
        }
    }

    sealed class Status(val viewDataBundle: ViewDataBundle) {
        class Loading(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class SignInSuccess(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class HaveUserLoginOnThisDevice(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class UpdateUI(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class Error(val networkError: Throwable, viewDataBundle: ViewDataBundle) :
            Status(viewDataBundle)
    }

    data class ViewDataBundle(val account: IAccount? = null)

}