package com.example.unknown.viewModel.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.network.Resource
import com.example.network.ResponseHandler
import com.example.network.repository.AdsRepository
import com.example.network.repository.UnknownRepository
import com.example.network.response.UserResponse
import com.example.unknown.viewModel.DisposableViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainViewModel(private val repo: UnknownRepository) : DisposableViewModel() {

    private val mutableStatus = MutableLiveData<Status>()

    val status: LiveData<Status> = mutableStatus

    private val currentValue: Status
        get() = mutableStatus.value!!

    init {
        mutableStatus.value =
            Status.Loading(
                ViewDataBundle()
            )
    }

    fun getAllUser(page: Int = 0) {
        mutableStatus.value = Status.Loading(currentValue.viewDataBundle)
        addDisposable(
            repo.getAllUser(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mutableStatus.value =
                        Status.Success(
                            ViewDataBundle(
                                it.data!!.toMutableList(), it.ads
                            )
                        )
                }, {
                    mutableStatus.value =
                        Status.Error(
                            ResponseHandler().handleException(it as Exception),
                            currentValue.viewDataBundle
                        )
                })
        )

    }

    fun getSingleUserNotFound() {
        mutableStatus.value = Status.Loading(currentValue.viewDataBundle)
        addDisposable(
            repo.getSingleUserNotFound()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mutableStatus.value = Status.Success(ViewDataBundle())
                }, {
                    mutableStatus.value = Status.Error(
                        ResponseHandler().handleException(it as Exception),
                        currentValue.viewDataBundle
                    )
                })
        )
    }

    sealed class Status(val viewDataBundle: ViewDataBundle) {
        class Loading(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class Success(viewDataBundle: ViewDataBundle) : Status(viewDataBundle)
        class Error(val networkError: Resource<String>, viewDataBundle: ViewDataBundle) :
            Status(viewDataBundle)
    }

    data class ViewDataBundle(
        val userResponse: MutableList<UserResponse> = arrayListOf(),
        val ads: AdsRepository? = null
    )
}