package com.example.unknown.extension

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import android.graphics.Color
import androidx.core.content.ContextCompat
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView
import com.example.unknown.R
import org.joda.time.LocalDate
import org.joda.time.LocalTime

fun LocalDate.defaultFormat(): String = this.toString("yyyy-MM-dd")

fun LocalTime.defaultFormat(): String = this.toString("HH:mm")

fun org.threeten.bp.LocalDate.defaultFormat(): String =
    this.format(org.threeten.bp.format.DateTimeFormatter.ofPattern("yyyy-MM-dd"))

fun org.threeten.bp.LocalTime.defaultFormat(): String =
    this.format(org.threeten.bp.format.DateTimeFormatter.ofPattern("HH:mm"))

/*fun toBounds(center: LatLng, radiusInMeters: Double): LatLngBounds {
    val distanceFromCenterToCorner = radiusInMeters * Math.sqrt(2.0)
    val southwestCorner = SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 225.0)
    val northeastCorner = SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 45.0)
    return LatLngBounds(southwestCorner, northeastCorner)
}*/

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

fun TextView.setPolicyString(callback: () -> Unit) {
    val stringPolicy = this.text.toString()
    val spannableString = SpannableString(stringPolicy)
    val clickableSpan = object : ClickableSpan() {
        override fun onClick(widget: View) {
            callback()
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.color = ContextCompat.getColor(context!!, R.color.colorPrimary)
            ds.isUnderlineText = false
        }
    }
    spannableString.setSpan(
        clickableSpan,
        stringPolicy.indexOf("  "),
        stringPolicy.length,
        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    this.movementMethod = LinkMovementMethod.getInstance()
    this.highlightColor = Color.TRANSPARENT
    this.text = spannableString
}

fun FragmentManager.cleanChildFragments() {
    val childFragments = this.fragments
    if (childFragments.isNotEmpty()) {
        val ft = this.beginTransaction()
        for (fragment in childFragments) {
            ft.remove(fragment)
        }
        ft.commit()
    }
}