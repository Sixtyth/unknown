package com.example.unknown.extension

import android.animation.Animator
import android.animation.ValueAnimator
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.DecelerateInterpolator
import kotlin.math.abs


fun View.toggle(
    onViewChange: ((percent: Float, isExpand: Boolean) -> Unit)? = null,
    duration: Int = 200,
    isForceExpand: Boolean = false
) {

    val isExpand = if (isForceExpand) {
        true
    } else {
        visibility != View.VISIBLE
    }
    val targetHeight = if (isExpand) {
        val widthSpec =
            View.MeasureSpec.makeMeasureSpec((parent as View).width, View.MeasureSpec.EXACTLY)
        val heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        measure(widthSpec, heightSpec)
        measuredHeight
    } else {
        0
    }
    val prevHeight = height
    visibility = View.VISIBLE
    val valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight)
    val maxValue = abs(prevHeight - targetHeight) * 1f
    valueAnimator.addUpdateListener { animation ->
        val value = animation.animatedValue
        layoutParams.height = value as Int
        var percent: Float = if (maxValue != 0.0f) {
            value / maxValue
        } else {
            1.0f
        }
        if (percent > 1) {
            percent = 1.0f
        }
        alpha = percent
        requestLayout()
        onViewChange?.invoke(percent, isExpand)
    }
    valueAnimator.addListener(object : Animator.AnimatorListener {
        override fun onAnimationStart(animation: Animator) {
            if (isExpand) {
                visibility = View.VISIBLE
            }
            animation.duration = 1L
        }

        override fun onAnimationEnd(animation: Animator) {
            if (!isExpand) {
                visibility = View.INVISIBLE
            }
        }

        override fun onAnimationCancel(animation: Animator) {

        }

        override fun onAnimationRepeat(animation: Animator) {

        }
    })
    valueAnimator.interpolator = DecelerateInterpolator()
    valueAnimator.duration = duration.toLong()
    valueAnimator.start()

}

fun View.setOnClickWithAnimationListener(onViewChange: (() -> Unit)? = null) {

    this.setOnClickListener {
        it.startAnimation(buttonClick)
        onViewChange!!.invoke()
    }

}

private val buttonClick = AlphaAnimation(1f, 0f)