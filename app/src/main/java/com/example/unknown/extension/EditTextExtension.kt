package com.example.unknown.extension

import android.content.Context
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.example.unknown.R

fun EditText.hideKeyboard() {
    val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
    if (hasFocus()) {
        this.clearFocus()
    }
}

fun EditText.showKeyboard() {
    this.requestFocus()
    val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun TextInputEditText.validateNonEmpty(): Boolean {
    val field = this.text.toString().trim()
    if (this.parentForAccessibility is TextInputLayout) {
        val parent = this.parentForAccessibility as TextInputLayout
        if (field.isEmpty()) {
            parent.isErrorEnabled = true
            parent.error = this.context!!.getString(R.string.error_required, parent.hint.toString())
            return false
        } else {
            parent.isErrorEnabled = false
        }
    } else {
        if (field.isEmpty()) {
            this.error = this.context!!.getString(R.string.error_required, hint)
            return false
        } else {
            this.error = ""
        }
    }
    return true
}

fun EditText.validateNonEmpty(): Boolean {
    val field = this.text.toString().trim()
    if (field.isEmpty()) {
        this.error = this.context!!.getString(R.string.error_required, hint)
        return false
    }
    return true
}