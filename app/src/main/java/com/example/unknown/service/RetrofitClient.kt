package com.example.unknown.service

import com.example.network.AuthenticationInterceptor
import com.example.network.Supplier
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.example.unknown.BuildConfig
import com.example.network.UnknownApi
import com.example.network.UnknownAuthenticatedApi
import com.example.unknown.utils.UserInfo
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

fun provideOkHttpClient(): OkHttpClient {
    val builder = OkHttpClient.Builder()
        .connectTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
    return builder.build()
}

fun provideAuthenticatedOkHttpClient(): OkHttpClient {
    val builder = OkHttpClient.Builder()
        .addInterceptor(AuthenticationInterceptor(object : Supplier<String> {
            override fun get(): String {
                return UserInfo.token
            }
        }))
    return builder.build()
}

fun provideAuthenticatedRetrofit(httpOk: OkHttpClient, gson: Gson): UnknownAuthenticatedApi {
    val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.HOST)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(httpOk)
        .build()
    return retrofit.create(UnknownAuthenticatedApi::class.java)
}

fun provideRetrofit(httpOk: OkHttpClient, gson: Gson): UnknownApi {
    val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.HOST)
        .client(httpOk)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    return retrofit.create(UnknownApi::class.java)
}

fun provideGson(): Gson {
    return GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
}


fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val logger = HttpLoggingInterceptor()
    logger.level = HttpLoggingInterceptor.Level.BASIC
    return logger
}
