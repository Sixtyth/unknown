// Generated by view binder compiler. Do not edit!
package com.example.unknown.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.example.unknown.R;
import com.example.unknown.ui.views.NeomorphFrameLayout;
import com.example.unknown.ui.views.PinEntryView;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityPinCodeBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final Button btnEight;

  @NonNull
  public final Button btnFive;

  @NonNull
  public final Button btnFour;

  @NonNull
  public final Button btnNine;

  @NonNull
  public final Button btnOne;

  @NonNull
  public final ImageView btnRemove;

  @NonNull
  public final Button btnSeven;

  @NonNull
  public final Button btnSix;

  @NonNull
  public final Button btnThree;

  @NonNull
  public final Button btnTwo;

  @NonNull
  public final Button btnZero;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutEight;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutFive;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutFour;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutNine;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutOne;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutRemove;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutSeven;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutSix;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutThree;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutTwo;

  @NonNull
  public final NeomorphFrameLayout neomorphFrameLayoutZero;

  @NonNull
  public final PinEntryView pinEntryView;

  private ActivityPinCodeBinding(@NonNull ConstraintLayout rootView, @NonNull Button btnEight,
      @NonNull Button btnFive, @NonNull Button btnFour, @NonNull Button btnNine,
      @NonNull Button btnOne, @NonNull ImageView btnRemove, @NonNull Button btnSeven,
      @NonNull Button btnSix, @NonNull Button btnThree, @NonNull Button btnTwo,
      @NonNull Button btnZero, @NonNull NeomorphFrameLayout neomorphFrameLayoutEight,
      @NonNull NeomorphFrameLayout neomorphFrameLayoutFive,
      @NonNull NeomorphFrameLayout neomorphFrameLayoutFour,
      @NonNull NeomorphFrameLayout neomorphFrameLayoutNine,
      @NonNull NeomorphFrameLayout neomorphFrameLayoutOne,
      @NonNull NeomorphFrameLayout neomorphFrameLayoutRemove,
      @NonNull NeomorphFrameLayout neomorphFrameLayoutSeven,
      @NonNull NeomorphFrameLayout neomorphFrameLayoutSix,
      @NonNull NeomorphFrameLayout neomorphFrameLayoutThree,
      @NonNull NeomorphFrameLayout neomorphFrameLayoutTwo,
      @NonNull NeomorphFrameLayout neomorphFrameLayoutZero, @NonNull PinEntryView pinEntryView) {
    this.rootView = rootView;
    this.btnEight = btnEight;
    this.btnFive = btnFive;
    this.btnFour = btnFour;
    this.btnNine = btnNine;
    this.btnOne = btnOne;
    this.btnRemove = btnRemove;
    this.btnSeven = btnSeven;
    this.btnSix = btnSix;
    this.btnThree = btnThree;
    this.btnTwo = btnTwo;
    this.btnZero = btnZero;
    this.neomorphFrameLayoutEight = neomorphFrameLayoutEight;
    this.neomorphFrameLayoutFive = neomorphFrameLayoutFive;
    this.neomorphFrameLayoutFour = neomorphFrameLayoutFour;
    this.neomorphFrameLayoutNine = neomorphFrameLayoutNine;
    this.neomorphFrameLayoutOne = neomorphFrameLayoutOne;
    this.neomorphFrameLayoutRemove = neomorphFrameLayoutRemove;
    this.neomorphFrameLayoutSeven = neomorphFrameLayoutSeven;
    this.neomorphFrameLayoutSix = neomorphFrameLayoutSix;
    this.neomorphFrameLayoutThree = neomorphFrameLayoutThree;
    this.neomorphFrameLayoutTwo = neomorphFrameLayoutTwo;
    this.neomorphFrameLayoutZero = neomorphFrameLayoutZero;
    this.pinEntryView = pinEntryView;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityPinCodeBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityPinCodeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_pin_code, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityPinCodeBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    String missingId;
    missingId: {
      Button btnEight = rootView.findViewById(R.id.btnEight);
      if (btnEight == null) {
        missingId = "btnEight";
        break missingId;
      }
      Button btnFive = rootView.findViewById(R.id.btnFive);
      if (btnFive == null) {
        missingId = "btnFive";
        break missingId;
      }
      Button btnFour = rootView.findViewById(R.id.btnFour);
      if (btnFour == null) {
        missingId = "btnFour";
        break missingId;
      }
      Button btnNine = rootView.findViewById(R.id.btnNine);
      if (btnNine == null) {
        missingId = "btnNine";
        break missingId;
      }
      Button btnOne = rootView.findViewById(R.id.btnOne);
      if (btnOne == null) {
        missingId = "btnOne";
        break missingId;
      }
      ImageView btnRemove = rootView.findViewById(R.id.btnRemove);
      if (btnRemove == null) {
        missingId = "btnRemove";
        break missingId;
      }
      Button btnSeven = rootView.findViewById(R.id.btnSeven);
      if (btnSeven == null) {
        missingId = "btnSeven";
        break missingId;
      }
      Button btnSix = rootView.findViewById(R.id.btnSix);
      if (btnSix == null) {
        missingId = "btnSix";
        break missingId;
      }
      Button btnThree = rootView.findViewById(R.id.btnThree);
      if (btnThree == null) {
        missingId = "btnThree";
        break missingId;
      }
      Button btnTwo = rootView.findViewById(R.id.btnTwo);
      if (btnTwo == null) {
        missingId = "btnTwo";
        break missingId;
      }
      Button btnZero = rootView.findViewById(R.id.btnZero);
      if (btnZero == null) {
        missingId = "btnZero";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutEight = rootView.findViewById(R.id.neomorphFrameLayoutEight);
      if (neomorphFrameLayoutEight == null) {
        missingId = "neomorphFrameLayoutEight";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutFive = rootView.findViewById(R.id.neomorphFrameLayoutFive);
      if (neomorphFrameLayoutFive == null) {
        missingId = "neomorphFrameLayoutFive";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutFour = rootView.findViewById(R.id.neomorphFrameLayoutFour);
      if (neomorphFrameLayoutFour == null) {
        missingId = "neomorphFrameLayoutFour";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutNine = rootView.findViewById(R.id.neomorphFrameLayoutNine);
      if (neomorphFrameLayoutNine == null) {
        missingId = "neomorphFrameLayoutNine";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutOne = rootView.findViewById(R.id.neomorphFrameLayoutOne);
      if (neomorphFrameLayoutOne == null) {
        missingId = "neomorphFrameLayoutOne";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutRemove = rootView.findViewById(R.id.neomorphFrameLayoutRemove);
      if (neomorphFrameLayoutRemove == null) {
        missingId = "neomorphFrameLayoutRemove";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutSeven = rootView.findViewById(R.id.neomorphFrameLayoutSeven);
      if (neomorphFrameLayoutSeven == null) {
        missingId = "neomorphFrameLayoutSeven";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutSix = rootView.findViewById(R.id.neomorphFrameLayoutSix);
      if (neomorphFrameLayoutSix == null) {
        missingId = "neomorphFrameLayoutSix";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutThree = rootView.findViewById(R.id.neomorphFrameLayoutThree);
      if (neomorphFrameLayoutThree == null) {
        missingId = "neomorphFrameLayoutThree";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutTwo = rootView.findViewById(R.id.neomorphFrameLayoutTwo);
      if (neomorphFrameLayoutTwo == null) {
        missingId = "neomorphFrameLayoutTwo";
        break missingId;
      }
      NeomorphFrameLayout neomorphFrameLayoutZero = rootView.findViewById(R.id.neomorphFrameLayoutZero);
      if (neomorphFrameLayoutZero == null) {
        missingId = "neomorphFrameLayoutZero";
        break missingId;
      }
      PinEntryView pinEntryView = rootView.findViewById(R.id.pinEntryView);
      if (pinEntryView == null) {
        missingId = "pinEntryView";
        break missingId;
      }
      return new ActivityPinCodeBinding((ConstraintLayout) rootView, btnEight, btnFive, btnFour,
          btnNine, btnOne, btnRemove, btnSeven, btnSix, btnThree, btnTwo, btnZero,
          neomorphFrameLayoutEight, neomorphFrameLayoutFive, neomorphFrameLayoutFour,
          neomorphFrameLayoutNine, neomorphFrameLayoutOne, neomorphFrameLayoutRemove,
          neomorphFrameLayoutSeven, neomorphFrameLayoutSix, neomorphFrameLayoutThree,
          neomorphFrameLayoutTwo, neomorphFrameLayoutZero, pinEntryView);
    }
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
