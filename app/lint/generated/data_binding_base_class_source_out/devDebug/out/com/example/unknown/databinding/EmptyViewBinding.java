// Generated by view binder compiler. Do not edit!
package com.example.unknown.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import com.example.unknown.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class EmptyViewBinding implements ViewBinding {
  @NonNull
  private final FrameLayout rootView;

  @NonNull
  public final Button btnAction;

  @NonNull
  public final Button btnFlat;

  @NonNull
  public final FrameLayout flContent;

  @NonNull
  public final ImageView ivImage;

  @NonNull
  public final LinearLayout llEmpty;

  @NonNull
  public final ProgressBar loadingIndicator;

  @NonNull
  public final TextView tvMessage;

  @NonNull
  public final TextView tvTitle;

  private EmptyViewBinding(@NonNull FrameLayout rootView, @NonNull Button btnAction,
      @NonNull Button btnFlat, @NonNull FrameLayout flContent, @NonNull ImageView ivImage,
      @NonNull LinearLayout llEmpty, @NonNull ProgressBar loadingIndicator,
      @NonNull TextView tvMessage, @NonNull TextView tvTitle) {
    this.rootView = rootView;
    this.btnAction = btnAction;
    this.btnFlat = btnFlat;
    this.flContent = flContent;
    this.ivImage = ivImage;
    this.llEmpty = llEmpty;
    this.loadingIndicator = loadingIndicator;
    this.tvMessage = tvMessage;
    this.tvTitle = tvTitle;
  }

  @Override
  @NonNull
  public FrameLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static EmptyViewBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static EmptyViewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.empty_view, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static EmptyViewBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    String missingId;
    missingId: {
      Button btnAction = rootView.findViewById(R.id.btnAction);
      if (btnAction == null) {
        missingId = "btnAction";
        break missingId;
      }
      Button btnFlat = rootView.findViewById(R.id.btnFlat);
      if (btnFlat == null) {
        missingId = "btnFlat";
        break missingId;
      }
      FrameLayout flContent = rootView.findViewById(R.id.flContent);
      if (flContent == null) {
        missingId = "flContent";
        break missingId;
      }
      ImageView ivImage = rootView.findViewById(R.id.ivImage);
      if (ivImage == null) {
        missingId = "ivImage";
        break missingId;
      }
      LinearLayout llEmpty = rootView.findViewById(R.id.llEmpty);
      if (llEmpty == null) {
        missingId = "llEmpty";
        break missingId;
      }
      ProgressBar loadingIndicator = rootView.findViewById(R.id.loadingIndicator);
      if (loadingIndicator == null) {
        missingId = "loadingIndicator";
        break missingId;
      }
      TextView tvMessage = rootView.findViewById(R.id.tvMessage);
      if (tvMessage == null) {
        missingId = "tvMessage";
        break missingId;
      }
      TextView tvTitle = rootView.findViewById(R.id.tvTitle);
      if (tvTitle == null) {
        missingId = "tvTitle";
        break missingId;
      }
      return new EmptyViewBinding((FrameLayout) rootView, btnAction, btnFlat, flContent, ivImage,
          llEmpty, loadingIndicator, tvMessage, tvTitle);
    }
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
